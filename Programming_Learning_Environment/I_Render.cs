﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Programming_Learning_Environment
{
    /// <summary>Abstract class defining the methods for the output of user draw commands to the GUI</summary>
    public interface I_Render
    {
    //Basic pen controls
        /// <summary>Method for changing the current drawing coordinates</summary>
        /// <param name="x_ord">Integer number of horizontal pixels from the origin (top left) of drawing area</param>
        /// <param name="y_ord">Integer number of vertical pixels from the origin (top left) of drawing area</param>
        void MoveTo(int x_ord, int y_ord);
        /// <summary>Method for drawing a point-to-point vector from the current drawing coordinates to those specified</summary>
        /// <remarks>Line drawn using the current Pen and associated properties <see cref="SetColour(Color)"/></remarks>
        /// <param name="x_ord">To draw to the integer number of horizontal pixels from the origin</param>
        /// <param name="y_ord">To draw to the integer number of horizontal pixels from the origin</param>
        void DrawTo(int x_ord, int y_ord);
        //Advanced pen controls
        /// <summary>Method for changing the Color property of the program's pen, created in concrete class <see cref="Render"/></summary>
        /// <param name="colour"><see cref="System.Drawing.Color"/> which the program should draw in</param>
        void SetColour(Color colour);
        /// <summary>Method to set a flag associated with drawing.<br/><br/>
        /// True = solid fill of drawn shape<br/>
        /// False = outline of drawn shape only</summary>
        /// <param name="toFill">Boolean flag, to fill or not to fill</param>
        void SetFill(Boolean toFill);

    //Basic shapes
        /// <summary>Method for outputting a user defined circle to the GUI</summary>
        /// <param name="radius">Circle radius defined as an integer number of pixels</param>
        void Circle(int radius);
        /// <summary>Method for outputting a user defined right-angled triangle to the GUI</summary>
        /// <param name="a_side">Triangle width defined as an integer number of pixels</param>
        /// <param name="b_side">Triangle height defined as an integer number of pixels</param>
        void Triangle(int a_side, int b_side);
        /// <summary>Method for outputting a user defined rectangle to the GUI</summary>
        /// <param name="a_side">Rectangle width defined as an integer number of pixels</param>
        /// <param name="b_side">Rectangle height defined as an integer number of pixels</param>
        void Rectangle(int a_side, int b_side);
    //Advanced shape
        /// <summary>Method for outputting a user defined polygon to the GUI</summary>
        /// <remarks>Polygon is expressed as a sequence of free-form user-defined coordinates. Polygon does not start form the current drawing coordinates and is not affected by <see cref="MoveTo(int, int)"/></remarks>
        /// <param name="points">Integer number of corners/poits/vertices polygon will have</param>
        /// <param name="coordinates">Key-value pair of integers functioning as a list of coordinates.<br/>
        /// Key = X-ordinate<br/>
        /// Value = Y-ordinate</param>
        void Polygon(int points, List<KeyValuePair<int, int>> coordinates);

    //Basic canvas controls
        /// <summary>Method for returing drawing area of GUI to it's original state</summary>
        void Clear();
        /// <summary>Method for returning drawing area and current drawing oordinates to their original states</summary>
        void Reset();

    }
}
