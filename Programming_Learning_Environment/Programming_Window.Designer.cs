﻿
namespace Programming_Learning_Environment
{
    partial class Programming_Window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Display = new System.Windows.Forms.PictureBox();
            this.Command_Line = new System.Windows.Forms.TextBox();
            this.Pointer_Location = new System.Windows.Forms.Label();
            this.Program_Code = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Display)).BeginInit();
            this.SuspendLayout();
            // 
            // Display
            // 
            this.Display.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Display.BackColor = System.Drawing.SystemColors.Window;
            this.Display.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Display.Location = new System.Drawing.Point(484, 12);
            this.Display.Name = "Display";
            this.Display.Size = new System.Drawing.Size(768, 576);
            this.Display.TabIndex = 0;
            this.Display.TabStop = false;
            // 
            // Command_Line
            // 
            this.Command_Line.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Command_Line.BackColor = System.Drawing.SystemColors.WindowText;
            this.Command_Line.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.Command_Line.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Command_Line.ForeColor = System.Drawing.SystemColors.Window;
            this.Command_Line.Location = new System.Drawing.Point(484, 642);
            this.Command_Line.Name = "Command_Line";
            this.Command_Line.Size = new System.Drawing.Size(768, 27);
            this.Command_Line.TabIndex = 2;
            this.Command_Line.Text = "[ENTER COMMAND]";
            this.Command_Line.Click += new System.EventHandler(this.CmdLine_Click);
            this.Command_Line.KeyDown += new System.Windows.Forms.KeyEventHandler(this.CmdLine_IfEnter);
            this.Command_Line.Leave += new System.EventHandler(this.CmdLine_IfBlank);
            // 
            // Pointer_Location
            // 
            this.Pointer_Location.AutoSize = true;
            this.Pointer_Location.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Pointer_Location.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Pointer_Location.Location = new System.Drawing.Point(824, 591);
            this.Pointer_Location.Name = "Pointer_Location";
            this.Pointer_Location.Size = new System.Drawing.Size(122, 16);
            this.Pointer_Location.TabIndex = 3;
            this.Pointer_Location.Text = "<to be replaced>";
            // 
            // Program_Code
            // 
            this.Program_Code.AcceptsReturn = true;
            this.Program_Code.AcceptsTab = true;
            this.Program_Code.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Program_Code.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.Program_Code.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Program_Code.Location = new System.Drawing.Point(12, 12);
            this.Program_Code.Multiline = true;
            this.Program_Code.Name = "Program_Code";
            this.Program_Code.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.Program_Code.Size = new System.Drawing.Size(466, 657);
            this.Program_Code.TabIndex = 1;
            this.Program_Code.WordWrap = false;
            // 
            // Programming_Window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.Pointer_Location);
            this.Controls.Add(this.Command_Line);
            this.Controls.Add(this.Program_Code);
            this.Controls.Add(this.Display);
            this.Name = "Programming_Window";
            this.Text = "Programming Environment";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Quit);
            ((System.ComponentModel.ISupportInitialize)(this.Display)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Display;
        private System.Windows.Forms.TextBox Command_Line;
        private System.Windows.Forms.Label Pointer_Location;
        private System.Windows.Forms.TextBox Program_Code;
    }
}

