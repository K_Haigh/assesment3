﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Programming_Learning_Environment
{
    /// <summary>Concrete class implementing Render Interface.</summary>
    public class Render : I_Render
    {
        /// <summary>Object identifier for the GUI element to be output to</summary>
        private Graphics canvass;
        /// <summary>Object identifier for the program's Pen</summary>
        private Pen pen;
        /// <summary>Object identifeirs maintaining the current drawing loaction</summary>
        private int x_ord, y_ord;
        /// <summary>Object identifier for the drawing fill flag</summary>
        private Boolean filling;

        /// <summary>Constructor:<br/>
        /// Initialises <see cref="canvass"/> to the given GUI component's graphics context, as well as the default starting state of the drawing functionality - <br/><br/>
        /// Drawing coordinates = (0,0)<br/>
        /// Fill shapes = false<br/>
        /// Pen is black</summary>
        /// <param name="component"><see cref="Graphics"/> context of GUI control that will display <see cref="Render"/> output</param>
        public Render (Graphics component)
        {
            canvass = component;
            x_ord = 0;
            y_ord = 0;
            filling = false;
            pen = Factory.MakeBlackPen();

            Clear();
        }

        /// <inheritdoc cref="I_Render.Circle(int)"/>
        /// <remarks><see cref="Graphics.DrawEllipse(Pen, int, int, int, int)"/> takes major and minor axis as arguments - the greater and lesser diameters of an elipse.<br/>
        /// For a circle, these diameters must be equal. They are also double the radius input</remarks>
        public void Circle(int radius)
        {
            if (! filling)
            {
                canvass.DrawEllipse(pen, x_ord, y_ord, 2*radius, 2*radius);
            }
            if (filling)
            {
                canvass.FillEllipse(pen.Brush, x_ord, y_ord, 2*radius, 2*radius);
            }

        }

        /// <inheritdoc cref="I_Render.Clear"/>
        public void Clear()
        {
            canvass.Clear(Color.White);
        }

        /// <inheritdoc cref="I_Render.DrawTo(int, int)"/>
        public void DrawTo(int x_ord, int y_ord)
        {
            canvass.DrawLine(pen, this.x_ord, this.y_ord, x_ord, y_ord);
            MoveTo(x_ord, y_ord);
        }

        /// <inheritdoc cref="I_Render.MoveTo(int, int)"/>
        public void MoveTo(int x_ord, int y_ord)
        {
            this.x_ord = x_ord;
            this.y_ord = y_ord;
        }

        /// <inheritdoc cref="I_Render.Polygon(int, List{KeyValuePair{int, int}})"/>
        public void Polygon(int points, List<KeyValuePair<int, int>> coordinates)
        {
            int count = 0;
            Point[] vertices = Factory.MakePointArray(points);
            
            foreach (KeyValuePair<int, int> vertex in coordinates)
            {
                vertices[count] = Factory.MakePoint(coordinates[count].Key, coordinates[count].Value);
                count++;
            }

            if (!filling)
            {
                canvass.DrawPolygon(pen, vertices);
            }
            if (filling)
            {
                canvass.FillPolygon(pen.Brush, vertices);
            }
        }

        /// <inheritdoc cref="I_Render.Rectangle(int, int)"/>
        public void Rectangle(int a_side, int b_side)
        {
            if (!filling)
            {
                canvass.DrawRectangle(pen, x_ord, y_ord, a_side, b_side);
            }
            if (filling)
            {
                canvass.FillRectangle(pen.Brush, x_ord, y_ord, a_side, b_side);
            }
        }

        /// <inheritdoc cref="I_Render.Reset"/>
        public void Reset()
        {
            Clear();
            x_ord = 0;
            y_ord = 0;
        }

        /// <inheritdoc cref="I_Render.SetColour(Color)"/>
        public void SetColour(Color colour)
        {
            pen.Color = colour;
        }

        /// <inheritdoc cref="I_Render.SetFill(bool)"/>
        public void SetFill(bool toFill)
        {
            filling = toFill;
        }

        
    //Basic triangle
        /// <inheritdoc cref="I_Render.Triangle(int, int)"/>
        public void Triangle(int width, int height)
        {
            Point[] abc = Factory.MakePointArray(3);
            abc[0] = Factory.MakePoint(x_ord, y_ord);
            abc[1] = Factory.MakePoint(x_ord + width, y_ord);
            abc[2] = Factory.MakePoint(x_ord, y_ord + height);

            if (!filling)
            {
                canvass.DrawPolygon(pen, abc);
            }
            if (filling)
            {
                canvass.FillPolygon(pen.Brush, abc);
            }
        }
    //Advanced triangle
        /// <summary>Advanced triangle method, overloading <see cref="Triangle(int, int)"/><br/><br/>
        /// Triangle with a horizontal base, two user defined side lengths and an interior angle</summary>
        /// <param name="a_side">Integer width of triangle in pixels. Always parallel to X-axis</param>
        /// <param name="b_side">Length of side freely defined in space. NOT triangle height</param>
        /// <param name="angle">Interior angle between defined sides, measured in whole degrees</param>
        public void Triangle(int a_side, int b_side, int angle)
        {
            Point[] abc = Factory.MakePointArray(3);
            abc[0] = Factory.MakePoint(x_ord, y_ord);
            abc[1] = Factory.MakePoint(x_ord + a_side, y_ord);
            double radianAngle = 2*Math.PI*(angle/360.0);

            int cX = (int)Math.Round(x_ord + b_side * Math.Cos(radianAngle));
            int cY = (int)Math.Round(y_ord + b_side * Math.Sin(radianAngle));
            abc[2] = Factory.MakePoint(cX, cY);

            if (!filling)
            {
                canvass.DrawPolygon(pen, abc);
            }
            if (filling)
            {
                canvass.FillPolygon(pen.Brush, abc);
            }
        }


    // Getter methods not intended to deliver program functionality. For use with Unit Testing.
    // As of 9/1/2022 - can be removed in release version
    #pragma warning disable CS1591 //Intentionally not XML documented.
        public Graphics GetGraphics()
        {
            return canvass;
        }
        public Pen GetPen()
        {
            return pen;
        }
        public int GetX_ord()
        {
            return x_ord;
        }
        public int GetY_ord()
        {
            return y_ord;
        }
        public Boolean GetFilling()
        {
            return filling;
        }
    }
}
