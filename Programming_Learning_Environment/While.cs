﻿using System;
using System.Data;

namespace Programming_Learning_Environment
{
    /// <summary>Concrete class inheriting from <see cref="I_Command"/> specifically implementing conditional loop execution functionality</summary>
    public class While : I_Command
    {
        /// <summary>Constructor:<br/>
        /// Calls override of <see cref="I_Command.ParseCommand(string, Render)"/> method, returning the error code for the while-statement when object is created</summary>
        /// <param name="lineIn">String of code deamed to be parsed as a while-statement </param>
        /// <param name="output">Drawing implementation object</param>
        /// <param name="errorCode">While-statement return code expressed as a string</param>
        public While(string lineIn, Render output, out string errorCode)
        {
            errorCode = ParseCommand(lineIn, output);
        }


        ///<inheritdoc cref="I_Command.ParseCommand(string, Render)"/>
        ///<remarks>While-statement specific implementation<br/>
        ///See <see cref="Interpreter.InterpretLine(string)"/></remarks>
        protected override string ParseCommand(string lineIn, Render output)
        {
            string[] conditional = lineIn.Split(delimitCommand);
            Boolean execute = false;
            DataTable dataTable = new DataTable();


            //  0       1   2       3
            //  while   var cond    limit

            int variableValue = Factory.GetVariable( conditional[1] );
            int limitingValue = Int32.Parse( conditional[3] );

            string condition = variableValue + conditional[2] + limitingValue;


            //Go through each line of code, only executing after reading 'while' and before reading 'endloop'
            foreach (string line in Programming_Window.GetCode() )
            {
                if (line.Split(delimitCommand)[0].Trim().Equals("endloop"))
                {
                    //If end of loop reached, but condition still true, recursively execute the loop again (with user updated variables)
                    if (Boolean.Parse(dataTable.Compute(condition, "").ToString()))
                    {
                        ParseCommand(lineIn, output);
                    }
                    //Otehrwise no more calls, reached bottom of recursion
                    else
                    {
                        execute = false;
                    }

                }

                //No execution before reading 'while...' command
                if (execute)
                {
                    Programming_Window.GetInstancePW().RunCommand( line.Trim() );
                }

                //Enable execution (literally) while the condition evaluates true.
                if (line.Split(delimitCommand)[0].Equals("while"))
                {
                    if ( Boolean.Parse( dataTable.Compute( condition, "" ).ToString() ) )
                    {
                        execute = true;
                    }
                    else
                    {
                        execute = false; //Else no more execution, no more recursive method calls, able to fulfill the recursion.
                    }
                }
            }

            return "ignore";
        }
    }
}
