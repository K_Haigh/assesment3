﻿using System;

namespace Programming_Learning_Environment
{
    /// <summary>Abstract class defining the structure of a 'variable' object, including relevant data</summary>
    public abstract class I_Variable
    {
        /// <summary>Seperating character between 'command' and 'data', as defined by 'code' syntax</summary>
        protected readonly char[] delimitCommand = Factory.MakeCharacterArray(' ');

        /// <summary>Method defining that all 'variable' objects should parse user input 'code' into meaningful C# and implement the intended function in C#</summary>
        /// <remarks>Runtime based errors are caught using Try-catch blocks.<br/>
        /// Command and Syntax checking asertains whether the correct data is present for the given command. It does not process the data thus does not catch context based errors - such as incomplete coordinates or missing values.</remarks>
        /// <param name="lineIn">String of 'code' for parsing</param>
        /// <returns>Return code expressed as a string</returns>
        protected abstract string ParseVariable(string lineIn);

        /// <summary>
        /// Method defining that all 'variable' objects must be able to return a contained integer value</summary>
        /// <returns>Integer value held as current value of variable</returns>
        public abstract int GetVariable();

        /// <summary>Method defining that all 'variable' objects must be able to update a contained integer</summary>
        /// <param name="value">Integer value to be set as current value of variable</param>
        public abstract void SetVariable(int value);

        ///<inheritdoc cref="Object.ToString"/>
        public abstract override string ToString();
    }
}
