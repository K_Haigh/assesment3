﻿using System;

namespace Programming_Learning_Environment
{
    /// <summary>Concrete class inheriting from <see cref="I_Command"/> specifically implementing rectangle drawing functionality</summary>
    public class Rectangle : I_Command
    {
        /// <summary>Constructor:<br/>
        /// Calls override of <see cref="I_Command.ParseCommand(string, Render)"/> method, returning the error code for the draw Rectangle command when object is created</summary>
        /// <param name="lineIn">String of code deamed to be parsed as a rectangle command</param>
        /// <param name="output">Drawing implementation object</param>
        /// <param name="errorCode">Rectangle return code expressed as a string</param>
        public Rectangle(string lineIn, Render output, out string errorCode)
        {
            errorCode = ParseCommand(lineIn, output);
        }



        ///<inheritdoc cref="I_Command.ParseCommand(string, Render)"/>
        ///<remarks>Rectangle specific implementation<br/>
        ///See <see cref="Interpreter.InterpretLine(string)"/></remarks>
        protected override string ParseCommand(string lineIn, Render output)
        {
            string data = lineIn.Split(delimitCommand, 2)[1];
            string[] dataPointsString = data.Split(delimitData);

            int[] dataPoints = Factory.MakeIntegerArray(dataPointsString.Length);
            int count = 0;

            foreach (string datum in dataPointsString)
            {
                dataPoints[count] = Int32.Parse(dataPointsString[count].Trim());
                count++;
            }

            try
            {
                output.Rectangle(dataPoints[0], dataPoints[1]);
                return "ignore";
            }
            catch (IndexOutOfRangeException) //Runtime error: incomplete length and height supplied
            {
                return  "Data: " + lineIn + "\n" +
                        "This data is incomplete for the method 'rectangle'. Two whole number lengths must be specified.";
            }
        }
    }
}
