﻿using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

//There shouldn't be a single <new> keyword anywhere else in the project* (*except for 'new Programming_Window', Singleton)
namespace Programming_Learning_Environment
{
    ///<summary>Factory class providing static referance to methods for the creation of required objects</summary>
    public static class Factory
    {   
        ///<summary>List of <see cref="Variable"/> objects defined by the user</summary>
        private static List<Variable> variables = new List<Variable>();
        ///<summary>List of <see cref="Method"/> objects defined by the user</summary>
        private static List<Method> methods = new List<Method>();


    //Object manufacturing
        ///<summary>Makes <see cref="Render"/> object</summary>
        ///<param name="g">Graphical output component's graphics context </param>
        ///<returns><see cref="Render"/> object</returns>
        public static Render MakeRender(Graphics g)
        {
            return new Render(g);
        }
        ///<summary>Makes <see cref="Interpreter"/> object</summary>
        ///<param name="g">Graphical output component's graphics context </param>
        ///<returns><see cref="Interpreter"/> object</returns>
        public static Interpreter MakeInterpreter(Graphics g)
        {
            return new Interpreter(g);
        }
        ///<summary>Makes <see cref="SaveFileDialog"/> object</summary>
        ///<returns><see cref="SaveFileDialog"/> object</returns>
        public static SaveFileDialog MakeSaveFileDialog()
        {
            return new SaveFileDialog();
        }
        ///<summary>Makes<see cref="OpenFileDialog"/> object</summary>
        ///<returns><see cref="OpenFileDialog"/> object</returns>
        public static OpenFileDialog MakeOpenFileDialog()
        {
            return new OpenFileDialog();
        }
        ///<summary>Makes <see cref="Pen"/> object, with default black <see cref="Color"/></summary>
        ///<returns><see cref="Pen"/> object</returns>
        public static Pen MakeBlackPen()
        {
            return new Pen(Color.Black);
        }
        ///<summary>Makes <see cref="Point"/> object</summary>
        ///<param name="x">X-ordinate, integer</param> <param name="y">Y-ordinate, integer</param>
        ///<returns><see cref="Point"/> object</returns>
        public static Point MakePoint(int x, int y)
        {
            return new Point(x, y);
        }
        ///<summary>Makes<see cref="Point"/> array</summary>
        ///<param name="size">Array length, integer</param>
        ///<returns><see cref="Point"/> array</returns>
        public static Point[] MakePointArray(int size)
        {
            return new Point[size];
        }
        ///<summary>Makes<see cref="KeyValuePair{TKey, TValue}"/> object, representing a coordinate pair</summary>
        ///<param name="x">X-ordinate value, integer</param> <param name="y">Y-ordinate value, integer</param>
        ///<returns><see cref="KeyValuePair{TKey, TValue}"/> object</returns>
        public static KeyValuePair<int, int> MakeCoordinate(int x, int y)
        {
            return new KeyValuePair<int, int>(x, y);
        }
        ///<summary>Makes <see cref="KeyValuePair{TKey, TValue}"/> <see cref="List{T}"/>, representing a coordinate points</summary>
        ///<returns><see cref="List{T}"/> of <see cref="KeyValuePair{TKey, TValue}"/></returns>
        public static List<KeyValuePair<int, int>> MakePointsList()
        {
            return new List<KeyValuePair<int, int>>();
        }
        ///<summary>Makes <see cref="int"/> array</summary>
        ///<param name="size">Array length</param>
        ///<returns><see cref="int"/> array</returns>
        public static int[] MakeIntegerArray(int size)
        {
            return new int[size];
        }
        ///<summary>Makes <see cref="char"/> array of one character, used in <see cref="string.Split(char[])"/></summary>
        ///<param name="character">Single character</param>
        ///<returns><see cref="char"/> array</returns>
        public static char[] MakeCharacterArray(char character)
        {
            return new char[] { character };
        }
        ///<summary>Makes <see cref="string"/> <see cref="List{T}"/></summary>
        ///<returns><see cref="List{T}"/> of <see cref="string"/></returns>
        public static List<string> MakeStringList()
        {
            return new List<string>();
        }
        ///<summary>Makes <see cref="DataTable"/> object</summary>
        ///<returns><see cref="DataTable"/> object</returns>
        public static DataTable MakeDataTable()
        {
            return new DataTable();
        }

    //Command manufacturing
        ///<summary>Makes <see cref="MoveTo"/> object, returns errorCode</summary>
        ///<param name="lineIn">Command line in</param> <param name="output"><see cref="Render"/></param>
        ///<returns>Return code, string</returns>
        public static string MakeMoveTo( string lineIn, Render output )
        {
            new MoveTo(lineIn, output, out string errorCode);
            return errorCode;
        }
        ///<summary>Makes <see cref="DrawTo"/> object, returns errorCode</summary>
        ///<param name="lineIn">Command line in</param> <param name="output"><see cref="Render"/></param>
        ///<returns>Return code, string</returns>
        public static string MakeDrawTo(string lineIn, Render output)
        {
            new DrawTo(lineIn, output, out string errorCode);
            return errorCode;
        }
        ///<summary>Makes <see cref="SetColour"/> object, returns errorCode</summary>
        ///<param name="lineIn">Command line in</param> <param name="output"><see cref="Render"/></param>
        ///<returns>Return code, string</returns>
        public static string MakeSetColour(string lineIn, Render output)
        {
            new SetColour(lineIn, output, out string errorCode);
            return errorCode;
        }
        ///<summary>Makes <see cref="MakeFill"/> object, returns errorCode</summary>
        ///<param name="lineIn">Command line in</param> <param name="output"><see cref="Render"/></param>
        ///<returns>Return code, string</returns>
        public static string MakeFill(string lineIn, Render output)
        {
            new Fill(lineIn, output, out string errorCode);
            return errorCode;
        }
        ///<summary>Makes <see cref="Circle"/> object, returns errorCode</summary>
        ///<param name="lineIn">Command line in</param> <param name="output"><see cref="Render"/></param>
        ///<returns>Return code, string</returns>
        public static string MakeCircle(string lineIn, Render output)
        {
            new Circle(lineIn, output, out string errorCode);
            return errorCode;
        }
        ///<summary>Makes <see cref="Triangle"/> object, returns errorCode</summary>
        ///<param name="lineIn">Command line in</param> <param name="output"><see cref="Render"/></param>
        ///<returns>Return code, string</returns>
        public static string MakeTriangle(string lineIn, Render output)
        {
            new Triangle(lineIn, output, out string errorCode);
            return errorCode;
        }
        ///<summary>Makes <see cref="Rectangle"/> object, returns errorCode</summary>
        ///<param name="lineIn">Command line in</param> <param name="output"><see cref="Render"/></param>
        ///<returns>Return code, string</returns>
        public static string MakeRectangle(string lineIn, Render output)
        {
            new Rectangle(lineIn, output, out string errorCode);
            return errorCode;
        }
        ///<summary>Makes <see cref="Polygon"/> object, returns errorCode</summary>
        ///<param name="lineIn">Command line in</param> <param name="output"><see cref="Render"/></param>
        ///<returns>Return code, string</returns>
        public static string MakePolygon(string lineIn, Render output)
        {
            new Polygon(lineIn, output, out string errorCode);
            return errorCode;
        }

    //Conditional manufacturing
        ///<summary>Makes <see cref="While"/> object, returns errorCode</summary>
        ///<param name="lineIn">Command line in</param> <param name="output"><see cref="Render"/></param>
        ///<returns>Return code, string</returns>
        public static string MakeWhileStatement(string lineIn, Render output)
        {
            new While(lineIn, output, out string errorCode);
            return errorCode;
        }
        ///<summary>Makes <see cref="If"/> object, returns errorCode</summary>
        ///<param name="lineIn">Command line in</param> <param name="output"><see cref="Render"/></param>
        ///<returns>Return code, string</returns>
        public static string MakeIfStatement(string lineIn, Render output)
        {
            new If(lineIn, output, out string errorCode);
            return errorCode;
        }

    //Manufacturing program variables
        ///<summary>Makes <see cref="Variable"/> object, returns errorCode</summary>
        ///<param name="lineIn">Command line in</param>
        ///<returns>Return code, string</returns>
        public static string MakeVariable(string lineIn)
        {
            variables.Add(new Variable(lineIn, out string errorCode) );
            return errorCode;
        }
        ///<summary>Gets variable value for given variable name</summary>
        ///<param name="variable">Variable name, string</param>
        ///<returns>Variable value, integer</returns>
        public static int GetVariable(string variable)
        {
            // Find 'Variable' object with the string name 'variable' and return the integer value of that
            return variables.Find(x => x.ToString().Equals(variable)).GetVariable() ;
        }
        ///<summary>Set variable value for given variable name</summary>
        ///<param name="variableName">Variable name, string</param> <param name="variableValue">Variable value, integer</param>
        public static void SetVariable(string variableName, int variableValue)
        {
            // Find 'Variable' object with the string name 'variable'
            Variable variable = variables.Find(x => x.ToString().Equals(variableName));
            variable.SetVariable(variableValue);
        }

        ///<summary>Makes <see cref="Method"/> object, returns errorCode</summary>
        ///<param name="lineIn">Command line in</param> <param name="output"><see cref="Render"/></param>
        ///<returns>Return code, string</returns>
        public static string MakeMethod(string lineIn, Render output)
        {
            methods.Add(new Method(lineIn, output, out string errorCode));
            return errorCode;
        }
        ///<summary>Runs the logic contained with in method, specifiied by name</summary>
        public static void ExecuteMethod(string method)
        {
            methods.Find(x => x.ToString().Equals(method)).ExecuteMethod();
        }


    //Specific data structures
        ///<summary>Makes array containing all valid commands</summary>
        ///<returns>Valid commands, string array</returns>
        public static string[] MakeCommandArray()
        {
            return new string[] { "moveto", "drawto", "setcolour", "fill", "circle", "triangle", "rectangle", "polygon", "clear", "reset" };
        }
        ///<summary>Makes array, containing all valid colours by name</summary>
        ///<returns>Valid <see cref="Color"/>, string array</returns>
        public static string[] MakeColourArray()
        {
            return new string[] { "aliceblue", "antiquewhite", "aqua", "aquamarine", "azure", "beige", "bisque", "black", "blanchedalmond", "blue", "blueviolet", "brown", "burlyWood", "cadetblue", "chartreuse", "chocolate", "coral", "cornflowerblue", "cornsilk", "crimson", "cyan", "darkblue", "darkcyan", "darkgoldenrod", "darkgray", "darkgreen", "darkkhaki", "darkmagenta", "darkolivegreen", "darkorange", "darkorchid", "darkred", "darksalmon", "darkseagreen", "darkslateblue", "darkslategray", "darkturquoise", "darkviolet", "deeppink", "deepskyblue", "dimgray", "dodgerblue", "firebrick", "floralwhite", "forestgreen", "fuchsia", "gainsboro", "ghostwhite", "gold", "goldenrod", "gray", "green", "greenyellow", "honeydew", "hotpink", "indianred", "indigo", "ivory", "khaki", "lavender", "lavenderblush", "lawngreen", "lemonchiffon", "lightblue", "lightcoral", "lightcyan", "lightgoldenrodyellow", "lightgray", "lightgreen", "lightpink", "lightsalmon", "lightseaGreen", "lightskyblue", "lightslategray", "lightsteelblue", "lightyellow", "lime", "limegreen", "linen", "magenta", "maroon", "mediumaquamarine", "mediumblue", "mediumorchid", "mediumpurple", "mediumseagreen", "mediumslateblue", "mediumspringgreen", "mediumturquoise", "mediumvioletred", "midnightblue", "mintcream", "mistyrose", "moccasin", "navajowhite", "navy", "oldlace", "olive", "olivedrab", "orange", "orangeRed", "orchid", "palegoldenrod", "palegreen", "paleturquoise", "palevioletRed", "papayawhip", "peachpuff", "peru", "pink", "plum", "powderblue", "purple", "red", "rosybrown", "royalblue", "saddlebrown", "salmon", "sandybrown", "seagreen", "seashell", "sienna", "silver", "skyblue", "slateblue", "slategray", "snow", "springgreen", "steelblue", "tan", "teal", "thistle", "tomato", "transparent", "turquoise", "violet", "wheat", "white", "whitesmoke", "yellow", "yellowgreen" /*and flashing*/, "redgreen", "blueyellow", "blackwhite" };
        }
        ///<summary>Makes array, containing all valid flashing colours, see <see cref="SetColour.colourFlasher"/></summary>
        ///<returns>Valid colour combinations, string array</returns>
        public static string[] MakeFlashArray()
        {
            return new string[] { "redgreen", "blueyellow", "blackwhite" };
        }
}
}
