﻿using System;
using System.Data;

namespace Programming_Learning_Environment
{
    /// <summary>Concrete class inheriting from <see cref="I_Command"/> specifically implementing conditional code execution functionality</summary>
    public class If : I_Command
    {
        /// <summary>Constructor:<br/>
        /// Calls override of <see cref="I_Command.ParseCommand(string, Render)"/> method, returning the error code for the if-statement when object is created</summary>
        /// <param name="lineIn">String of code deamed to be parsed as an if-statement </param>
        /// <param name="output">Drawing implementation object</param>
        /// <param name="errorCode">If-statement return code expressed as a string</param>
        public If(string lineIn, Render output, out string errorCode)
        {
            errorCode = ParseCommand(lineIn, output);
        }


        ///<inheritdoc cref="I_Command.ParseCommand(string, Render)"/>
        ///<remarks>If-statement specific implementation<br/>
        ///See <see cref="Interpreter.InterpretLine(string)"/></remarks>
        protected override string ParseCommand(string lineIn, Render output)
        {
            string[] conditional = lineIn.Split(delimitCommand);
            Boolean execute = false;
            DataTable dataTable = new DataTable();

           
            //  0   1   2       3
            //  if  var cond    value

            int variableValue = Factory.GetVariable(conditional[1]);
            string condition;
            int limitingValue = Int32.Parse(conditional[3]);
            
            //Note - dataTable.Compute() does not accept logical (== & !=) or literal (===) equality
            if ( conditional[2].Equals("==") || conditional[2].Equals("=="))
            {
                condition = variableValue + "=" + limitingValue;
            }
            // <, >, <= & >= are all OK
            else
            {
                condition = variableValue + conditional[2] + limitingValue;
            }



            //Go through each line of code, only executing after reading 'if' and before reading 'endif'
            foreach (string line in Programming_Window.GetCode())
            {
                //Must check if should not execute, before attempting to execute (see next if-statement)
                if (line.Split( delimitCommand, 2)[0].Trim().Equals("endif") )
                {
                    execute = false;
                }

                if (execute)
                {
                    Programming_Window.GetInstancePW().RunCommand(line.Trim());
                }

                //(literally) If the condition evaluates to true...
                if (line.Split(delimitCommand)[0].Equals("if") && line.Split(delimitCommand)[2].Equals("==") ) //See line 41
                {
                    //...Start executing lines
                    if ( Boolean.Parse( dataTable.Compute(condition, "").ToString() ) )
                    {
                        execute = true;
                    }
                    else
                    {
                        execute = false;
                    }
                }
                else if (line.Split(delimitCommand)[0].Equals("if") && line.Split(delimitCommand)[2].Equals("!=")) //See line 41
                {
                    //NOT equal to
                    if ( !Boolean.Parse(dataTable.Compute(condition, "").ToString() ) )
                    {
                        execute = true;
                    }
                    else
                    {
                        execute = false;
                    }
                }
                else if (line.Split(delimitCommand)[0].Equals("if") )
                {
                    if (Boolean.Parse(dataTable.Compute(condition, "").ToString()))
                    {
                        execute = true;
                    }
                    else
                    {
                        execute = false;
                    }
                }
            }

            return "ignore";
        }
    }
}