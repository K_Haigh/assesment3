﻿using System;

namespace Programming_Learning_Environment
{
    /// <summary>Concrete class inheriting from <see cref="I_Command"/> specifically implementing pen movement functionality</summary>
    public class MoveTo : I_Command
    {
        /// <summary>Constructor:<br/>
        /// Calls override of <see cref="I_Command.ParseCommand(string, Render)"/> method, returning the error code for the MoveTo command when object is created</summary>
        /// <param name="lineIn">String of code deamed to be parsed as a move to coordinates command</param>
        /// <param name="output">Drawing implementation object</param>
        /// <param name="errorCode">MoveTo return code expressed as a string</param>
        public MoveTo(string lineIn, Render output, out string errorCode)
        {
            errorCode = ParseCommand( lineIn, output);
        }



        ///<inheritdoc cref="I_Command.ParseCommand(string, Render)"/>
        ///<remarks>Move to given coordinates implementation<br/>
        ///See <see cref="Interpreter.InterpretLine(string)"/></remarks>
        protected override string ParseCommand(string lineIn, Render output)
        {
            string data = lineIn.Split(delimitCommand, 2)[1];
            string[] dataPointsString = data.Split(delimitData);

            int[] dataPoints = Factory.MakeIntegerArray(dataPointsString.Length);
            int count = 0;

            foreach (string datum in dataPointsString)
            {
                dataPoints[count] = Int32.Parse(dataPointsString[count].Trim());
                count++;
            }

            try
            {
                output.MoveTo(dataPoints[0], dataPoints[1]);
                return "ignore";
            }
            catch (IndexOutOfRangeException) //Runtime error: moveto x,<blank>
            {
                return  "Data: " + lineIn + "\n" +
                        "This data is incomplete for the method 'moveto'. A coordinate must be specified consisting of two whole numbers.";
            }
        }
    }
}
