﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programming_Learning_Environment
{
    /// <summary>
    /// Class containing root of program execution, as well as any program-wide or administrative methods
    /// </summary>
    static class Control
    {
        /// <summary>Execution entry point</summary>
        /// <remarks>Creates intance of Windows Forms GUI, user contol over program execution</remarks>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run( Factory.makeProgrammingWindow() );
        }
    }
}
