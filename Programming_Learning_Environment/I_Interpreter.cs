﻿using System;

namespace Programming_Learning_Environment
{
    /// <summary>Abstract class defining the methods for the parsing of user entered 'code' into C#</summary>
    public interface I_Interpreter
    {
        /// <summary>Method for the parsing of a single line of 'code' into implementation C# code<br/><br></br>
        /// Parsing should comprise of splitting a line into the command word and any accompanying data. The data should then be parsed into individual values.<br/>
        /// C# implementation code should reflect the parsed command and enact a function on or with the supplied values.</summary>
        /// <remarks>Returns a string dependant upon parsing success; containing either a success phrase or details about a run-time error.</remarks>
        /// <param name="lineIn">String of a single line of 'code' to be parsed into C#</param>
        /// <returns>Return code expressed as a string</returns>
        string InterpretLine(String lineIn);
        /// <summary>Method attempts to parse a single line of 'code' without parsing it to implementation C# code<br/>
        /// This method checks that the command can be parsed, but does implement any action.</summary>
        /// <remarks>Returns a string dependant upon parsing success; containing either a success phrase or details about a 'compile-time' error.</remarks>
        /// <param name="lineIn">String of a single line of 'code' to be checked for command parsability</param>
        /// <returns>Return code expressed as a string</returns>
        string CheckCommandLine(String lineIn);
        /// <summary>Method attempts to parse data passed into it from a parsed line of 'code' without passing it to C# implementation code<br/>
        /// Method checks that the data can be parsed, but does not process it with any logic</summary>
        /// <remarks>Returns a string dependant upon parsing success; containing either a success phrase or details about a 'compile-time' error.</remarks>
        /// <param name="lineIn">String of a single line of 'code' to be checked for data parsability</param>
        /// <returns>Return code expressed as a string</returns>
        string CheckCommandParameters(String lineIn);
    }
}