﻿using System;
using System.Windows.Forms;

namespace Programming_Learning_Environment
{
    /// <summary>Class containing root of program execution</summary>
    public class Start
    {
        /// <summary>Execution entry point</summary>
        /// <remarks>Creates intance of Windows Forms GUI, user contol over program execution</remarks>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run( Programming_Window.GetInstancePW() );
        }

    }
}
