﻿using System;

namespace Programming_Learning_Environment
{
    /// <summary>Concrete class inheriting from <see cref="I_Command"/> specifically implementing circle drawing functionality</summary>
    public class Circle : I_Command
    {
        /// <summary>Constructor:<br/>
        /// Calls override of <see cref="I_Command.ParseCommand(string, Render)"/> method, returning the error code for the draw Circle command when object is created</summary>
        /// <param name="lineIn">String of code deamed to be parsed as a circle command</param>
        /// <param name="output">Drawing implementation object</param>
        /// <param name="errorCode">Circle return code expressed as a string</param>
        public Circle(string lineIn, Render output, out string errorCode)
        {
            errorCode = ParseCommand(lineIn, output);
        }
        


        ///<inheritdoc cref="I_Command.ParseCommand(string, Render)"/>
        ///<remarks>Circle specific implementation<br/>
        ///See <see cref="Interpreter.InterpretLine(string)"/></remarks>
        protected override string ParseCommand(string lineIn, Render output)
        {
            string data = lineIn.Split(delimitCommand, 2)[1];

            output.Circle( Int32.Parse(data.Split(delimitData)[0].Trim() ) );
            return "ignore";
        }
    }
}
