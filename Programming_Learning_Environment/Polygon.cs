﻿using System;
using System.Collections.Generic;

namespace Programming_Learning_Environment
{
    /// <summary>Concrete class inheriting from <see cref="I_Command"/> specifically implementing polygon drawing functionality</summary>
    public class Polygon : I_Command
    {
        /// <summary>Constructor:<br/>
        /// Calls override of <see cref="I_Command.ParseCommand(string, Render)"/> method, returning the error code for the draw Polygon command when object is created</summary>
        /// <param name="lineIn">String of code deamed to be parsed as a polygon command</param>
        /// <param name="output">Drawing implementation object</param>
        /// <param name="errorCode">Polygon return code expressed as a string</param>
        public Polygon(string lineIn, Render output, out string errorCode)
        {
            errorCode = ParseCommand(lineIn, output);
        }



        ///<inheritdoc cref="I_Command.ParseCommand(string, Render)"/>
        ///<remarks>Circle specific implementation<br/>
        ///See <see cref="Interpreter.InterpretLine(string)"/></remarks>
        protected override string ParseCommand(string lineIn, Render output)
        {
            string data = lineIn.Split(delimitCommand, 2)[1];
            string[] dataPointsString = data.Split(delimitData);

            int[] dataPoints = Factory.MakeIntegerArray(dataPointsString.Length);
            int count = 0;

            foreach (string datum in dataPointsString)
            {
                dataPoints[count] = Int32.Parse(dataPointsString[count].Trim());
                count++;
            }

            List<KeyValuePair<int, int>> listVertices = Factory.MakePointsList();
            count = 1;

            while (count < dataPoints.Length)
            {
                try
                {
                    listVertices.Add(Factory.MakeCoordinate(dataPoints[count], dataPoints[count + 1]));
                    count += 2;
                }
                catch (IndexOutOfRangeException) //Runtime error: incomplete co-ordinate supplied
                {
                    //Forces state where declared number of sides does not match given number of coordinates (i.e. dataPoints[0] != countVertices)
                    count += 2;
                }
            }

            int countVertices = listVertices.Count;

            if (countVertices < 2) // Polygon must have at least two points
            {
                return  "Data: " + lineIn + "\n" +
                        "This data is incomplete for the method 'polygon'.\n" +
                        "At least five whole numbers must be given - a number of vertices and two coordinates, each a pair of whole numbers.\n\n" +
                        "Every additional vertex (shape corner) must be given as an additional pair of whole numbers.";
            }
            else if (dataPoints[0] != countVertices) // Number of vertices stated and number of points given could differ. 'Compile-time' checking does not catch data mis-match.
            {
                return  "Data: " + lineIn + "\n" +
                        "This data is incorrect for the described 'polygon'.\n\n" +
                        "The number of sides stated is " + dataPoints[0] + "\n" +
                        "The number of coordinated given is " + listVertices.Count;
            }
            else
            {
                output.Polygon(dataPoints[0], listVertices);
                return "ignore";
            }
        }
    }
}
