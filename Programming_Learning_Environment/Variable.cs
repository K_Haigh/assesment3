﻿using System;
using System.Data;

namespace Programming_Learning_Environment
{
    /// <summary>Concrete class inheriting from <see cref="I_Variable"/> implementing variable object functionality</summary>
    public class Variable : I_Variable
    {
        /// <summary>Name attribute of a Variable object, as a string</summary>
        private string variableName;
        /// <summary>Value attribute of a Variable object, as an integer</summary>
        private int variableValue;

        /// <summary>Constructor:<br/>
        /// Calls override of <see cref="I_Variable.ParseVariable(string)"/> method, returning the error code for variable creation when Variable object is created</summary>
        /// <param name="lineIn">String of code deamed to be parsed as a variable </param>
        /// <param name="errorCode">Varaible declaration return code expressed as a string</param>
        public Variable( string lineIn, out string errorCode)
        {
            errorCode = ParseVariable(lineIn);
        }



        ///<inheritdoc cref="I_Variable.GetVariable()"/>
        public override int GetVariable()
        {
            return variableValue;
        }

        ///<inheritdoc cref="I_Variable.ParseVariable(string)"/>
        protected override string ParseVariable(string lineIn)
        {
            DataTable data = Factory.MakeDataTable();
            variableName = lineIn.Split(delimitCommand, 3)[0];
            variableValue = Int32.Parse( data.Compute( lineIn.Split(delimitCommand, 3)[2], "" ).ToString() );
 
            return "ignore";
        }

        ///<inheritdoc cref="I_Variable.SetVariable(int)"/>
        public override void SetVariable(int value)
        {
            variableValue = value;
        }

        ///<inheritdoc cref="Object.ToString"/>
        public override string ToString() 
        {
            return variableName;
        }
    }
}
