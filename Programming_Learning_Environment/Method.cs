﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Programming_Learning_Environment
{
    /// <summary>Concrete class inheriting from <see cref="I_Variable"/> implementing user method functionality</summary>
    public class Method : I_Command
    {
        /// <summary>Name attribute of Method object, as a string</summary>
        private string methodName;
        /// <summary>List of command line strings, containing method logic</summary>
        private List<string> commandLines = Factory.MakeStringList();


        /// <summary>Constructor:<br/>
        /// Calls override of <see cref="I_Variable.ParseVariable(string)"/> method, returning the error code method creation when Method object is created</summary>
        /// <param name="lineIn">String of code deamed to be parsed as a method declaration </param>
        /// <param name="output">Drawing implementation object</param>
        /// <param name="errorCode">Method declaration return code expressed as a string</param>
        public Method(string lineIn, Render output, out string errorCode)
        {
            errorCode = ParseCommand(lineIn, output);
        }


        ///<inheritdoc cref="I_Command.ParseCommand(string, Render)"/>
        ///<remarks>Method declaration specific implementation<br/>
        ///See <see cref="Interpreter.InterpretLine(string)"/></remarks>
        protected override string ParseCommand(string lineIn, Render output)
        {
            DataTable dataTable = Factory.MakeDataTable();
            Boolean store = false;

            methodName = lineIn.Split(delimitCommand)[1];

            //Go through each line of code, only recording commands to the method after reading 'method <name>' and before reading 'endmethod'
            foreach (string line in Programming_Window.GetCode())
            {
                if (line.Split(delimitCommand)[0].Trim().Equals("endmethod"))
                {
                    commandLines.Add(line.Trim());
                    store = false;
                }

                //No recording before reading 'method...' command
                if (store)
                {
                    commandLines.Add(line.Trim());
                }

                //Record commands after 'method...' command as the method logic
                if (line.Trim().Equals(lineIn.Trim()))
                {
                    store = true;
                }
            }

            return "ignore";
        }

        /// <summary>Method to run the commands contained within the Method object</summary>
        public void ExecuteMethod()
        {
            foreach (string line in commandLines)
            {
                Programming_Window.GetInstancePW().RunCommand(line.Trim());
            }
        }

        ///<inheritdoc cref="Object.ToString"/>
        public override string ToString()
        {
            return methodName;
        }
    }
}
