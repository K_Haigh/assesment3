﻿using System;

namespace Programming_Learning_Environment
{
    /// <summary>Concrete class inheriting from <see cref="I_Command"/> specifically implementing triangle drawing functionality</summary>
    public class Triangle : I_Command
    {
        /// <summary>Constructor:<br/>
        /// Calls override of <see cref="I_Command.ParseCommand(string, Render)"/> method, returning the error code for the draw Triangle command when object is created</summary>
        /// <param name="lineIn">String of code deamed to be parsed as a triangle command</param>
        /// <param name="output">Drawing implementation object</param>
        /// <param name="errorCode">Triangle return code expressed as a string</param>
        public Triangle(string lineIn, Render output, out string errorCode)
        {
            errorCode = ParseCommand(lineIn, output);
        }



        ///<inheritdoc cref="I_Command.ParseCommand(string, Render)"/>
        ///<remarks>Triangle specific implementation<br/>
        ///See <see cref="Interpreter.InterpretLine(string)"/></remarks>
        protected override string ParseCommand(string lineIn, Render output)
        {
            string data = lineIn.Split(delimitCommand, 2)[1];
            string[] dataPointsString = data.Split(delimitData);

            int[] dataPoints = Factory.MakeIntegerArray(dataPointsString.Length);
            int count = 0;

            foreach (string datum in dataPointsString)
            {
                dataPoints[count] = Int32.Parse(dataPointsString[count].Trim());
                count++;
            }

            if (dataPoints.Length == 2) // Basic triangle
            {
                output.Triangle(dataPoints[0], dataPoints[1]);
                return "ignore";
            }
            else if (dataPoints.Length == 3) // Advanced triangle, with angle
            {
                output.Triangle(dataPoints[0], dataPoints[1], dataPoints[2]);
                return "ignore";
            }
            else //Runtime error: incomplete base and height given
            {
                return "Data: " + lineIn + "\n" +
                        "This data is incomplete for the method 'triangle'. At least two whole number lengths must be specified.\n" +
                        "Alternatively, two whole number lengths and a whole number angle, measured in degrees.";
            }
        }
    }
}
