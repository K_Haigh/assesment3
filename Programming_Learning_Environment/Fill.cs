﻿namespace Programming_Learning_Environment
{
    /// <summary>Concrete class inheriting from <see cref="I_Command"/> specifically implementing functionality to draw shapes as an outline or filled-in</summary>
    public class Fill : I_Command
    {
        /// <summary>Constructor:<br/>
        /// Calls override of <see cref="I_Command.ParseCommand(string, Render)"/> method, returning the error code for the set Fill command when object is created</summary>
        /// <param name="lineIn">String of code deamed to be parsed as a fill command</param>
        /// <param name="output">Drawing implementation object</param>
        /// <param name="errorCode">Fill return code expressed as a string</param>
        public Fill(string lineIn, Render output, out string errorCode)
        {
            errorCode = ParseCommand(lineIn, output);
        }



        ///<inheritdoc cref="I_Command.ParseCommand(string, Render)"/>
        ///<remarks>Fill shape specific implementation<br/>
        ///See <see cref="Interpreter.InterpretLine(string)"/></remarks>
        protected override string ParseCommand(string lineIn, Render output)
        {
            string data = lineIn.Split(delimitCommand, 2)[1];

            if (data.Trim().Equals("on"))
            {
                output.SetFill(true);
            }
            else if (data.Trim().Equals("off"))
            {
                output.SetFill(false);
            }
            return "ignore";
        }
    }
}
