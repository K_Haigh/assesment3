﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;

namespace Programming_Learning_Environment
{
    /// <summary>Concrete class implementing Interpreter interface</summary>
    public class Interpreter : I_Interpreter
    {
        /// <summary>Object identifier for drawing implementation</summary>
        private Render output;
        /// <summary>New line character, as defined by 'code' syntax</summary>
        readonly char[] delimitLine = Factory.MakeCharacterArray('\n');
        /// <summary>Seperating character between 'command' and 'data', as defined by 'code' syntax</summary>
        readonly char[] delimitCommand = Factory.MakeCharacterArray(' ');
        /// <summary>Next datum character, as defined by 'code' syntax</summary>
        readonly char[] delimitData = Factory.MakeCharacterArray(',');
        /// <summary>'Commands' defined by 'code' language</summary>
        readonly string[] commands = Factory.MakeCommandArray();
        /// <summary>Colours available within <see cref="Color"/></summary>
        readonly string[] colours = Factory.MakeColourArray();
        /// <summary>List of variables by name (string), as means of refereance</summary>
        /// <remarks>Actual Variable objects stroed in <see cref="Factory"/> class</remarks>
        private List<string> variables = Factory.MakeStringList();
        /// <summary>List of methods by name (string), as means of refereance</summary>
        /// <remarks>Actual Method objects stroed in <see cref="Factory"/> class</remarks>
        private List<string> methods = Factory.MakeStringList();
        /// <summary>DataTable object for accessing 'Compute' method</summary>
        private DataTable dataTable = Factory.MakeDataTable();
        /// <summary>Execution flag - all lines are interpreted, not all lines are enacted</summary>
        private Boolean shouldExecute = true;



        /// <summary>Constructor:<br/>
        /// Initialises <see cref="output"/> to the <see cref="Render"/> object that will handle drawing functionality</summary>
        /// <param name="component"><see cref="Graphics"/> context of the GUI control that will display drawing operations ot the user</param>
        public Interpreter(Graphics component)
        {
            output = Factory.MakeRender(component);
        }



    /***Syntax & error checking methods***/

        /// <inheritdoc cref="I_Interpreter.CheckCommandLine(string)"/>
        public string CheckCommandLine(string lineIn)
        {
            string[] test = lineIn.Split(delimitCommand);

        //Must check if variable first. If command contains variable assignment, will not match known commands (user defined variable name).
            if (test.Length > 2 && test[1].Equals("="))
            {
                return "ignore";
            }

        //Check for other commands with highly specific forms (i.e. do not follow structure: 'command' <space> 'data')
        //***This section must be added to for each specialised command***

            //Conditional loop
            else if (test.Length == 4 && test[0].Equals("while"))
            {
                //Check that "endloop" is written somewhere in the code to properly complete the loop
                foreach (string line in Programming_Window.GetCode())
                {
                    if (line.Trim().Equals("endloop"))
                    {
                        // 0    1   2       3
                        // if   var cond    value
                        if (variables.Contains(test[1]) && Int32.TryParse(test[3], out int result) && (test[2].Equals("<") || test[2].Equals(">") || test[2].Equals("<=") || test[2].Equals(">=")))
                        {
                            return "ignore";
                        }
                        else
                        {
                            return  "Line in: " + lineIn + "\n" +
                                    "This line does not follow the 'while' structure, see below.\n\n" +
                                    "while <variable> <comparator> <integer>";
                        }
                    }
                }
                return  "Line in: " + lineIn + "\n" +
                        "A while loop was declared, but never closed. Ensure code includes the \"endloop\" command.";
            }

            //Conditional execution
            else if (test.Length == 4 && test[0].Equals("if"))
            {
                //Check that "endif" is written somewhere in the code to properly complete the conditional code block
                foreach (string line in Programming_Window.GetCode())
                {
                    if (line.Trim().Equals("endif"))
                    {
                        // 0    1   2       3
                        // if   var cond    value
                        if (variables.Contains(test[1]) && Int32.TryParse(test[3], out int result) && (test[2].Equals("==") || test[2].Equals("!=") || test[2].Equals("<") || test[2].Equals(">") || test[2].Equals("<=") || test[2].Equals(">=")))
                        {
                            return "ignore";
                        }
                        else
                        {
                            return "Line in: " + lineIn + "\n" +
                                    "This line does not follow the 'if' structure, see below.\n\n" +
                                    "if <variable> == <integer>";
                        }
                    }
                }
                return  "Line in: " + lineIn + "\n" +
                        "A if statement was declared, but never closed. Ensure code includes the \"endif\" command.";
            }

            //Methods
            else if (test.Length == 2 && test[0].Equals("method"))
            {
                //When declaring a method...
                if (!methods.Contains(test[1]))
                {
                    //...check that "endmethod" is written somewhere in the code to properly complete the method block
                    foreach (string line in Programming_Window.GetCode())
                    {
                        if (line.Trim().Equals("endmethod"))
                        {
                            return "ignore";
                        }
                    }
                }
                else if (methods.Contains(test[1]))
                {
                    return "ignore";
                }
                return "Line in: " + lineIn + "\n" +
                        "A if statement was declared, but never closed. Ensure code includes the \"endif\" command.";
            }

            //Statement delimiters
            else if (test.Length == 1 && (test[0].Equals("endloop") || test[0].Equals("endif") || test[0].Equals("endmethod")))
            {
                return "ignore";
            }

            //Valid single word commands
            else if (test.Length == 1 && (test[0].Equals("reset") || test[0].Equals("clear") || variables.Contains(test[0].Trim())))
            {
                return "ignore";
            }


            //Begin testing for general command structure (i.e. do not follow structure: 'command' <space> 'data')
            //***This section should not need changing for commands following the general form.


            //Establish the rule that a line of code is: command <space> datum,datum,datum...
            else if (test.Length != 2 && !test[0].Equals("reset") && !test[0].Equals("clear") && !variables.Contains(test[0].Trim()))
            {
                return "Line: " + lineIn + "\n" +
                        "This line does not follow the structure 'command' <space> 'data'\n\n" +
                        "Correct examples would be: 'moveto 100,100' OR 'setcolour red'\n" +
                        "OR 'clear', where no data is required";
            }

            //If the command is parsable, but not recognised as a command or a variable, return error
            else if (!commands.Contains(test[0].Trim()) && !variables.Contains(test[0].Trim()))
            {
                return "Line: " + lineIn + "\n" +
                        "This line does not contain a valid command\n\n" +
                        "Valid command words are:\n" + String.Join(", ", commands) + "\n\n" +
                        "If attempting to declare a variable, ensure that each term in the variable daclaration is seperated by a space.";
            }

            // If the command was SetColour, the data cannot be an integer, return error
            else if (test[0].Equals("setcolour") && Int32.TryParse(test[1], out int success))
            {
                return "Line: " + lineIn + "\n" +
                        "This command cannot take whole numbers as input. A worded colour must be given.\n\n" +
                        "Valid colours can be found in help documentation, or online at:\n" +
                        "https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.colors";
            }

            // If the command is recognised and requires data, check the data for errors.
            else
                return CheckCommandParameters(lineIn);
        }

        /// <inheritdoc cref="I_Interpreter.CheckCommandParameters(string)"/>
        public string CheckCommandParameters(string lineIn)
        {
            // Whole input required for context when checking data
            string[] testLine = lineIn.Split(delimitCommand, 2);
            string dataIn = testLine[1];
            string[] test = dataIn.Split(delimitData);

            // On or Off is only valid for a Fill command
            if ((dataIn.Trim().Equals("on") || dataIn.Trim().Equals("off")) && testLine[0].Equals("fill"))
            {
                return "ignore";
            }

            //<colour> is only valid for a SetColour command
            else if (colours.Contains(dataIn.Trim().ToLower()) && testLine[0].Equals("setcolour")) // <colour> is only valid for a SetColour command
            {
                return "ignore";
            }

            //Colour data can only be used with SetColour command, otherwise invalid
            else if (colours.Contains(dataIn.Trim()) && !testLine[0].Equals("setcolour"))
            {
                return "Data: " + lineIn + "\n" +
                        "This command can only take whole numbers as arguments. Only \"setcolour\" can be used with recognised colours.";
            }

            //If the first datum cannot be parsed to an integer, the data cannot be valid. String data handled by the above if-statements
            else if (!Int32.TryParse(test[0], out int datum1))
            {
                //Except if valid variable
                if (variables.Contains(test[0]))
                {
                    return "variable";
                }

                return "Data: " + lineIn + "\n" +
                        "This is not valid data. It is not a recognised colour, or a whole number  written as an integer (1 or 2 or 10...).\n\n" +
                        "Valid colours can be found in help documentation, or online at:\n" +
                        "https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.colors";
            }

            //If the first datum is parsable to an integer, the rest should be too...
            else if (Int32.TryParse(test[0], out int datum2))
            {
                foreach (string datum in test)
                {
                    if (!Int32.TryParse(datum, out int intDatum))
                    {
                        //Except if valid variable
                        if (variables.Contains(datum.Trim()))
                        {
                            return "variable";
                        }

                        return "Data: " + lineIn + "\n\n" +
                                "The data entered does not entirely consist of whole numbers,  written as an integer (1 or 2 or 10...).\n" +
                                "Ensure every line only contains one type of data";
                    }
                }
                return "ignore";
            }

            //Should not be possible to reach this point via the GUI. May be possible with unit testing. Ensures there is always a returned string.
            else
                return "Data: " + lineIn + "\n\n" +
                        "The data entered was not recognised as a valid data type of this command" +
                        "Check the data type expected by the command you are tying to input " + dataIn + " into.";
        }

        /// <summary>Method attempts to parse command line for variable assignment to assertain whether the terms defined are sufficiant to complete the assignment</summary>
        /// /// <remarks>Returns a string dependant upon parsing success; containing either a success phrase or details about a 'compile-time' error.</remarks>
        /// <param name="lineIn">User entered command line as a string</param>
        /// <returns>Return code expressed as a string</returns>
        public string CheckVariableAssignment(string lineIn)
        {
            string[] assignment = lineIn.Split(delimitCommand);

            //If:   x = 10        (i.e. a straight variable assignment, word equals value) 
            //If:   x = 1+1       (i.e. variable assignment to a valid expression)
            if (assignment.Length == 3 && assignment[1].Equals("=") && assignment[2] != "")
            {
                string value = assignment[2];

                //If:   x = a     (where a is an existing variable)
                if (variables.Contains(assignment[2]))
                {
                    Factory.GetVariable(assignment[2]).ToString();
                    return "variable";
                }

                try
                {
                    Int32.Parse(dataTable.Compute(value, "").ToString());
                    return "ignore";
                }
                catch (EvaluateException)
                {
                    return "Variable expression: " + lineIn + "\n" +
                        "The expression to the right of the equality cannot be evaluated. Ensure the espression only contains integer numbers.\n\n" +
                        "If using a variable in an equation, always ensure it is seperated from any non-variable terms by spaces.";
                }
                catch (FormatException)
                {
                    return "Variable expression: " + lineIn + "\n" +
                            "This expression contains values that are not integers. Only whole numbers can be stored in variables.";
                }
            }

            //If:   x = 1 + 1     (i.e. expression seperated by spaces, the command delimiter)
            else if (assignment.Length > 3)
            {
                string expression = null;

                for (int i = 2; i < assignment.Length; i++)
                {
                    if (variables.Contains(assignment[i]))
                    {
                        return "variable";
                    }

                    expression = expression + assignment[i];
                }

                try
                {
                    Int32.Parse(dataTable.Compute(expression, "").ToString());
                    return "ignore";
                }

                catch (EvaluateException)
                {
                    return "Variable expression: " + lineIn + "\n" +
                            "The expression to the right of the equality cannot be evaluated. Ensure the espression only contains integer numbers and known variable names.\n\n" +
                            "If using a variable in an equation, always ensure it is seperated from any non-variable terms by spaces.";

                }
                catch (FormatException)
                {
                    return "Variable expression: " + lineIn + "\n" +
                            "This expression contains values that are not integers. Only whole numbers can be stored in variables.";
                }

            }

            //Don't think this execution path can actually be reached using the GUI.
            else
            {
                return "Variable assignment: " + lineIn + "\n" +
                        "The value to the right of the equality is not an integer that can be assigned to the name on the left.";
            }
        }



    /***User code execution methods***/

        /// <summary>Extension of <see cref="I_Interpreter.InterpretLine(string)"/><br/>
        /// Calls InterpretLine for each line in piece of 'code'.</summary>
        /// <param name="codeIn">Multiple lines of commands as a single string, delimited by newline characters</param>
        /// <returns>Return code of the Interpreted line</returns>
        public string InterpretCode(string codeIn)
        {
            string[] codeLines = codeIn.Split(delimitLine);
            string errorCode;

            foreach (string line in codeLines)
            {
                errorCode = InterpretLine(line); // Error checking is an integral part of InterpretLine, carrys over to InterpretCode

                if (!errorCode.Equals("ignore"))
                {
                    return errorCode;
                }
            }

            return "ignore";
        }

        /// <inheritdoc cref="I_Interpreter.InterpretLine(string)"/>
        /// <remarks><br/>Implementation uses selection statements to execute the correct handling of subsequent data, based in initial command. See <see cref="I_Command"/></remarks>
        public string InterpretLine(string lineIn)
        {
            string instruction = lineIn.Split(delimitCommand, 2)[0];
            string errorCode;

        /***Add (simple, 'command <space> data') function to program - create class of I_Command, add to factory, add to context switch***/

            if (shouldExecute)
            {
                switch (instruction)
                {
                //Drawing/visual commands
                    case "moveto":
                        return Factory.MakeMoveTo(lineIn, output);

                    case "drawto":
                        return Factory.MakeDrawTo(lineIn, output);

                    case "setcolour":
                        return Factory.MakeSetColour(lineIn, output);

                    case "fill":
                        return Factory.MakeFill(lineIn, output);

                    case "circle":
                        return Factory.MakeCircle(lineIn, output);

                    case "triangle":
                        return Factory.MakeTriangle(lineIn, output);

                    case "rectangle":
                        return Factory.MakeRectangle(lineIn, output);

                    case "polygon":
                        return Factory.MakePolygon(lineIn, output);

                //Simple commands, not necesary to create class of I_Command
                    case "clear":
                        output.Clear();
                        return "ignore";

                    case "reset":
                        output.Reset();
                        return "ignore";

                //Complex commands
                    case "while":
                        errorCode = Factory.MakeWhileStatement(lineIn, output);
                        shouldExecute = false;
                        return errorCode;

                    case "if":
                        errorCode = Factory.MakeIfStatement(lineIn, output);
                        shouldExecute = false;
                        return errorCode;

                    case "method":
                        if (methods.Contains(lineIn.Split(delimitCommand)[1]))
                        {
                            Factory.ExecuteMethod(lineIn.Split(delimitCommand)[1]);
                            return "ignore";
                        }
                        else
                        {
                            methods.Add(lineIn.Split(delimitCommand)[1]);
                            shouldExecute = false;
                            return Factory.MakeMethod(lineIn, output);
                        }

                    case "endloop":
                    case "endif":
                    case "endmethod":
                        return "ignore";

                //Variables
                    case string variable when variables.Contains(instruction):

                        errorCode = CheckVariableAssignment(lineIn);

                        //If function written as one, no spaces (i.e. 1+1)
                        if (errorCode.Equals("ignore") && lineIn.Split(delimitCommand).Length == 3)
                        {
                            Factory.SetVariable(variable, Int32.Parse(dataTable.Compute(lineIn.Split(delimitCommand)[2], "").ToString()));

                            return errorCode;
                        }

                        //Assigning one variable the value of another (i.e. b = a, where a is an existing variable)
                        if (errorCode.Equals("variable") && lineIn.Split(delimitCommand).Length == 3)
                        {
                            Factory.SetVariable(variable, Int32.Parse(dataTable.Compute(ReplaceVariableInLine(lineIn.Split(delimitCommand)[2]), "").ToString()));

                            return "ignore";
                        }

                        //If function written with spaces (i.e. 1 + 1)
                        else if (errorCode.Equals("ignore") && lineIn.Split(delimitCommand).Length >= 3)
                        {
                            Factory.SetVariable(variable, Int32.Parse(dataTable.Compute(ReplaceVariableInExpression(lineIn), "").ToString()));

                            return errorCode;
                        }

                        //Assigning one variable some function of another (i.e. b = a + 1, where a is an existing variable)
                        else if (errorCode.Equals("variable") && lineIn.Split(delimitCommand).Length >= 3)
                        {
                            Factory.SetVariable(variable, Int32.Parse(dataTable.Compute(ReplaceVariableInExpression(lineIn), "").ToString()));

                            return "ignore";
                        }

                        //If not declaring new variable value, but checking the value
                        else if (lineIn.Split(delimitCommand).Length == 1)
                        {
                            return "The current value of variable \"" + variable + "\" is:\n" +
                                    Factory.GetVariable(variable);
                        }

                        else
                        {
                            return errorCode;
                        }
                }

            //Equation functionality
                switch (lineIn.Split(delimitCommand)[1])
                {
                    // If the fist part of command (switched above) is not recognised, but the second part is "=", the user is creating a new variable
                    case "=":
                        errorCode = CheckVariableAssignment(lineIn);

                        if (errorCode.Equals("ignore"))
                        {
                            variables.Add(instruction);
                            return Factory.MakeVariable(lineIn);
                        }
                        else if (errorCode.Equals("variable"))
                        {
                            string lineInMod = ReplaceVariableInLine(lineIn);

                            variables.Add(instruction);
                            return Factory.MakeVariable(lineInMod);
                        }
                        else
                        {
                            return errorCode;
                        }

                    // Should not be possible to reach this point via the GUI. May be possible with unit testing. Ensures there is always a returned string.
                    default:
                        return "Code line: " + lineIn + "\n" +
                                "This line of code is completely unrecognised. Congratulations, this output should never be seen!";
                }
            }
            //Re-enable main execution once a statement delimiter is encountered (i.e. subroutine complete)
            else
            {
                switch (instruction)
                {
                    case "endloop":
                    case "endif":
                    case "endmethod":
                        shouldExecute = true;
                        return "ignore";
                }  
            }

            return "ignore";
        }



    /***Variable implementation methods***/

        /// <summary>Method for replacing named variables with variables value in line data , without altering the command</summary>
        /// <remarks>Command line structure: command 'space' datum,datum,datum... </remarks>
        /// <param name="lineIn">Entire command line string to be evaluated</param>
        /// <returns>Command line string, but with numerical data (variable names replaced)</returns>
        public string ReplaceVariableInCommand(string lineIn)
        {
            List<string> datumsOut = new List<string>();
            string dataOut;

            string[] datumsIn = lineIn.Split(delimitCommand)[1].Split(delimitData);

            //Replace any instances of variable names with variable value
            for (int i = 0; i < datumsIn.Length; i++)
            {
                if (variables.Contains(datumsIn[i]))
                {
                    datumsOut.Add(Factory.GetVariable(datumsIn[i]).ToString());
                }
                else
                {
                    datumsOut.Add(datumsIn[i]);
                }
            }

            dataOut = string.Join(",", datumsOut);

            return lineIn.Split(delimitCommand)[0] + " " + dataOut;
        }

        /// <summary>Method for replacing named variables with variable values in a given mathematical expression</summary>
        /// <param name="expressionIn">Data string to be evaluated, to the right of an equality</param>
        /// <returns>Expression string, but with numerical data (variable names replaced)</returns>
        public string ReplaceVariableInExpression(string expressionIn)
        {
            string[] termsIn = expressionIn.Split(delimitCommand);
            string expression = null;

            for (int i = 2; i < termsIn.Length; i++)
            {
                if (variables.Contains(expressionIn.Split(delimitCommand)[i]))
                {
                    expression = expression + Factory.GetVariable(termsIn[i]).ToString() + " ";
                }
                else
                {
                    expression = expression + termsIn[i] + " ";
                }
            }

            return expression;
        }

        /// <summary>Method for replacing all named variables in a line with their value</summary>
        /// <param name="lineIn">Line to be evaluated</param>
        /// <returns>Line string, but with only numerical data (all variable names replaced)</returns>
        public string ReplaceVariableInLine(string lineIn)
        {
            string[] termsIn = lineIn.Split(delimitCommand);
            string line = null;

            for (int i = 0; i < termsIn.Length; i++)
            {
                if (variables.Contains(lineIn.Split(delimitCommand)[i]))
                {
                    line = line + Factory.GetVariable(termsIn[i]).ToString() + " ";
                }
                else
                {
                    line = line + termsIn[i] + " ";
                }
            }

            return line;
        }
    }
}