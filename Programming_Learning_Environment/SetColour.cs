﻿using System;
using System.Drawing;
using System.Linq;
using System.Threading;

namespace Programming_Learning_Environment
{
    /// <summary>Concrete class inheriting from <see cref="I_Command"/> specifically implementing functionality to set drawing colour</summary>
    public class SetColour : I_Command
    {
        /// <summary>String array containing referace list of non-standard, 'flashing colours'</summary>
        private string[] flashingColours = Factory.MakeFlashArray();
        /// <summary>Object identifier for thread in case of use of flashing colours</summary>
        static Thread colourFlasher;

        /// <summary>Constructor:<br/>
        /// Calls override of <see cref="I_Command.ParseCommand(string, Render)"/> method, returning the error code for the SetColour command when object is created</summary>
        /// <param name="lineIn">String of code deamed to be parsed as a change colour command</param>
        /// <param name="output">Drawing implementation object</param>
        /// <param name="errorCode">SetColour return code expressed as a string</param>
        public SetColour(string lineIn, Render output, out string errorCode)
        {
            errorCode = ParseCommand(lineIn, output);
        }



        ///<inheritdoc cref="I_Command.ParseCommand(string, Render)"/>
        ///<remarks>Change drawing colour implementation<br/>
        ///See <see cref="Interpreter.InterpretLine(string)"/></remarks>
        protected override string ParseCommand(string lineIn, Render output)
        {
            string data = lineIn.Split(delimitCommand, 2)[1];

            //To change flashing colour, stop existing instance of thread
            if ( colourFlasher != null )
            {
                colourFlasher.Abort();
                Console.WriteLine("Thread terminated");

            }

            //Set a flashing colour. Or more accurately, alternating pen colour.
            if (flashingColours.Contains(data))
            {
                colourFlasher = new Thread( () => SetFlashing(data, output) );
                colourFlasher.Start();
            }

            //Set static pen colour
            else
            {
                output.SetColour(Color.FromName(data.Trim()));
            }

            return "ignore";
        }

        /// <summary>Method to be executed on a seperate thread, to constantly alternate between two given pen colours</summary>
        /// <param name="data">Non-standard colour name as a string</param>
        /// <param name="output">Drawing implementation object</param>
        public void SetFlashing(string data, Render output)
        {

            if ( data.Equals("redgreen") )
            {
                while (true)
                {
                    output.SetColour(Color.Red);
                    Console.WriteLine("Red");
                    Thread.Sleep(500);
                    output.SetColour(Color.Green);
                    Console.WriteLine("Green");
                    Thread.Sleep(500);
                }
            }
            else if (data.Equals("blueyellow"))
            {
                while (true)
                {
                    output.SetColour(Color.Blue);
                    Console.WriteLine("Blue");
                    Thread.Sleep(500);
                    output.SetColour(Color.Yellow);
                    Console.WriteLine("Yellow");
                    Thread.Sleep(500);
                }
            }
            else if (data.Equals("blackwhite"))
            {
                while (true)
                {
                    output.SetColour(Color.Black);
                    Console.WriteLine("Black");
                    Thread.Sleep(500);
                    output.SetColour(Color.White);
                    Console.WriteLine("White");
                    Thread.Sleep(500);
                }
            }
        }
    }
}
