﻿using System;
using System.Windows.Forms;

namespace Programming_Learning_Environment
{
    /// <summary>Singleton Class that creates instance of Windows Form user interface, and defines the handling of events thrown by relevant form contols</summary>
    /// <remarks>Partial class, partners IDE generated code. <see cref="Programming_Window.InitializeComponent">Generated Code</see>
    /// <para>Inherits from System.Windows.Forms.Form</para></remarks>
    public sealed partial class Programming_Window : Form
    {
        /// <summary>Refernace pointer to the single, static instance of Programming Windown</summary>
        private static readonly Programming_Window instancePW = new Programming_Window();
        /// <summary>Referance pointer to object responcible for holding user 'code' </summary>
        private static string[] linesIn;

        /// <summary>Referance pointer to object responcible for command parser</summary>
        private Interpreter draw;
        /// <summary>Referance pointer to object responcible for file output operations</summary>
        private SaveFileDialog save;
        /// <summary>Referance pointer to object responcible for file input operations</summary>
        private OpenFileDialog open;


        /// <summary>Constructor:<br/>
        ///Initialises display output source to 'Display' picturebox graphics context.<br/>
        ///Initialises I/O objects and filename/type filters</summary>
        private Programming_Window()
        {
            InitializeComponent();
            draw = Factory.MakeInterpreter(Display.CreateGraphics());
            save = Factory.MakeSaveFileDialog();
            open = Factory.MakeOpenFileDialog();

            save.Filter = "Text files (*.txt)|*.txt";
            open.Filter = "Text files (*.txt)|*.txt";
        }

        /// <summary>Method to access the Singleton of Programming_Window</summary>
        /// <returns>The sole instance of Programming Window object</returns>
        public static Programming_Window GetInstancePW()
        {
            return instancePW;
        }

        /// <summary>Method to access the user entered code contained within Singleton Programming_Window</summary>
        /// <returns>String array containing one complete line of code per index</returns>
        public static string[] GetCode()
        {
            return linesIn;
        }



        /// <summary>Method for executing user command, including error/syntax checking</summary>
        /// <param name="lineIn">String of one complete user command</param>
        /// <returns>Boolean - true = command line was run, false = command line fails</returns>
        public Boolean RunCommand(string lineIn)
        {
            lineIn = lineIn.Trim();

            string errorCode = draw.CheckCommandLine(lineIn); // Mandatory input validation

            //If command contains variable referance by name, replace with variable value
            if (errorCode.Equals("variable"))
            {
                lineIn = draw.ReplaceVariableInCommand(lineIn);

                errorCode = draw.CheckCommandLine(lineIn);
            }

            //If there is a line interpretation error, report error and do not execute
            if (!errorCode.Equals("ignore"))
            {
                Message(errorCode, "Command Line Error");

                return false;
            }

            //Execute command
            else
            {
                errorCode = draw.InterpretLine(lineIn);

                //Report run-time errors
                if (!errorCode.Equals("ignore"))
                {
                    Message(errorCode, "Run-time Error");

                    return false;
                }
                else
                {
                    this.Command_Line.Text = "";
                }

                return true;
            }
        }



        /// <summary>Handling code in the event user enters a command in the command line text box<br/>
        /// Is the primary contol mechanism for the end user. Command and Syntax checking is automatic and mandatory</summary>
        /// <remarks>Direct commands:<br/>
        /// RUN = execute lines in the program code text box<br/>
        /// SAVE = output contents of program code text box to a .txt file<br/>
        /// LOAD = input content of a .txt file to the progam code text box<br/><br/>
        /// Indirect commands = entering valid program code, line-by-line</remarks>
        /// <param name="sender">Control that triggered the event</param>
        /// <param name="e">Refereances event data</param>
        private void CmdLine_IfEnter(object sender, KeyEventArgs e)
        {
            //Function 1 - Running single lines of code via command line
            if (e.KeyCode == Keys.Enter && !this.Command_Line.Text.Equals("") && !this.Command_Line.Text.Equals("RUN") && !this.Command_Line.Text.Equals("SAVE") && !this.Command_Line.Text.Equals("LOAD"))
            {
                RunCommand(Command_Line.Text.ToLower());
            }

            //Function 2 - Running multiple lines of code, from code area
            else if (e.KeyCode == Keys.Enter && this.Command_Line.Text.Equals("RUN"))
            {
                linesIn = Program_Code.Text.ToLower().Split(Factory.MakeCharacterArray('\n'));

                Boolean canRun;
                int i = 0;

                //Will execute 'code' until the first error is encountered
                do
                {
                    if (i < linesIn.Length)
                    {
                        canRun = RunCommand(linesIn[i]);
                        i++;
                    }
                    else
                    {
                        canRun = false;
                    }
                }
                while (canRun);

            }
            //Function 3.1 - Save code content
            else if (e.KeyCode == Keys.Enter && this.Command_Line.Text.Equals("SAVE"))
            {
                if (save.ShowDialog() == DialogResult.OK)
                {
                    System.IO.File.WriteAllText(save.FileName, this.Program_Code.Text);
                }
            }
            //Function 3.2 - Load code content
            else if (e.KeyCode == Keys.Enter && this.Command_Line.Text.Equals("LOAD"))
            {
                if (open.ShowDialog() == DialogResult.OK)
                {
                    this.Program_Code.Text = System.IO.File.ReadAllText(open.FileName);
                }
            }

        }

        /// <summary>Method for displaying messages to the user - primarily the output of command and sysntax error checks - as a message box</summary>
        /// <param name="messageIn">Content to be displayed to message box body</param>
        /// <param name="messageType">Title of message box</param>
        private void Message(string messageIn, string messageType)
        {
            MessageBox.Show(messageIn, messageType);
        }

        /// <summary>Handling code in the event user clicks to type in the command line.<br/>
        /// Clears command line textbox of text</summary>
        /// <param name="sender">Control that triggered the event</param>
        /// <param name="e">Refereances event data</param>
        private void CmdLine_Click(object sender, EventArgs e)
        {
            this.Command_Line.Text = "";
        }

        /// <summary>Handling code in the event user clicks elsewhere, shifting event focus away form the command line<br/>
        /// Reasserts default text box content - [ENTER COMMAND]</summary>
        /// <param name="sender">Control that triggered the event</param>
        /// <param name="e">Refereances event data</param>
        private void CmdLine_IfBlank(object sender, EventArgs e)
        {
            if (this.Command_Line.Text.Equals(""))
            {
                this.Command_Line.Text = "[enter command]";
            }
        }

        /// <summary>Method to terminate all threads associated with the Windows Form when user 'Quits' the window </summary>
        /// <param name="sender">Control that triggered the event</param>
        /// <param name="e">Refereances event data</param>
        private void Quit(object sender, FormClosedEventArgs e)
        {
            Environment.Exit(Environment.ExitCode);
        }





        // Not intended to deliver program functionality. For use with Unit Testing.
        // As of 9/1/2022 - can be removed in release version
#pragma warning disable CS1591 //Intentionally not XML documented.
        public void SetCode(string linesIn)
        {
            Program_Code.Text = linesIn;

            Programming_Window.linesIn = Program_Code.Text.ToLower().Split(Factory.MakeCharacterArray('\n'));
        }
    }
}
