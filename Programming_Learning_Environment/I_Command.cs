﻿namespace Programming_Learning_Environment
{
    /// <summary>Abstract class defining the structure of a 'command' object, including relevant data</summary>
    public abstract class I_Command
    {
        /// <summary>Seperating character between 'command' and 'data', as defined by 'code' syntax</summary>
        protected readonly char[] delimitCommand = Factory.MakeCharacterArray(' ');
        /// <summary>Next datum character, as defined by 'code' syntax</summary>
        protected readonly char[] delimitData = Factory.MakeCharacterArray(',');

        /// <summary>Method defining that all 'command' objects should parse user input 'code' into meaningful C# and implement the intended function in C#</summary>
        /// <remarks>Runtime based errors are caught using Try-catch blocks.<br/>
        /// Command and Syntax checking asertains whether the correct data is present for the given command. It does not process the data thus does not catch context based errors - such as incomplete coordinates or missing values.</remarks>
        /// <param name="lineIn">String of 'code' for parsing</param>
        /// <param name="output">Drawing implementation object</param>
        /// <returns>Return code expressed as a string</returns>
        protected abstract string ParseCommand(string lineIn, Render output);
    }
}
