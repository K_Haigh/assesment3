﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Programming_Learning_Environment;
using System.Windows.Forms;
using System.Drawing;

namespace Test_PLE
{
    //Class testing 'Render' class for all base/part 1 drawing functionality
    [TestClass]
    public class RenderTesting
    {
        //For robust testing of methods that should react to input form a given range.
        Random rng = new Random();

        [TestMethod]
        public void Render_CorrectCreation()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            System.Drawing.Pen testPen = new System.Drawing.Pen(Color.Black);
            Graphics testGraphics = testControl.CreateGraphics();

            //Create graphics output object
            Render testRender = new Render(testGraphics);

            //Test intitial object state
            Assert.AreEqual(testRender.GetGraphics(), testGraphics, "Graphics context not returned from output class correctly.");
            Assert.AreEqual(testRender.GetPen().Color, testPen.Color, "Pen not correctly initialised in output class");
            Assert.AreEqual(testRender.GetX_ord(), 0, "Horizontal starting point not correctly initialised in output class");
            Assert.AreEqual(testRender.GetY_ord(), 0, "Vertical starting point not correctly initialised in output class");
            Assert.AreEqual(testRender.GetFilling(), false, "Shape outline drawing not correctly initialised in output class");
        }

        [TestMethod]
        public void MoveTo_Moves()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Test current internal coordinates
            Render testRender = new Render(testGraphics);
            Assert.AreEqual(testRender.GetX_ord(), 0, "Move method has not changed the default x-ordinate");
            Assert.AreEqual(testRender.GetY_ord(), 0, "Move method has not changed the default y-ordinate");

            //Test moved internal coordinates
            testRender.MoveTo(200, 100);
            Assert.AreEqual(testRender.GetX_ord(), 200, "Move method does not move to the correct x-ordinate");
            Assert.AreEqual(testRender.GetY_ord(), 100, "Move method does not move to the correct y-ordinate");
        }

        [TestMethod]
        public void MoveTo_MultipleMove()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Test current internal coordinates
            Render testRender = new Render(testGraphics);
            testRender.MoveTo(200, 100);

            Assert.AreNotEqual(testRender.GetX_ord(), 0, "Move method has not changed the default x-ordinate");
            Assert.AreNotEqual(testRender.GetY_ord(), 0, "Move method has not changed the default y-ordinate");

            //Test moved internal coordinates
            testRender.MoveTo(381, 245);
            Assert.AreEqual(testRender.GetX_ord(), 381, "Move method does not move to the correct x-ordinate");
            Assert.AreEqual(testRender.GetY_ord(), 245, "Move method does not move to the correct y-ordinate");
        }

        [TestMethod]
        public void DrawTo_Moves()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Test start point
            Render testRender = new Render(testGraphics);
            testRender.MoveTo(200, 100);
            Assert.AreEqual(testRender.GetX_ord(), 200, "Move method does not move to the correct x-ordinate");
            Assert.AreEqual(testRender.GetY_ord(), 100, "Move method does not move to the correct y-ordinate");

            //Test end point
            testRender.DrawTo(381, 245);
            Assert.AreEqual(testRender.GetX_ord(), 381, "Draw method does not move to the correct x-ordinate");
            Assert.AreEqual(testRender.GetY_ord(), 245, "Draw method does not move to the correct y-ordinate");
        }

        [TestMethod]
        public void DrawTo_MultiplePoints()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Test current internal coordinates
            Render testRender = new Render(testGraphics);

            Assert.AreEqual(testRender.GetX_ord(), 0, "Move method has not changed the default x-ordinate");
            Assert.AreEqual(testRender.GetY_ord(), 0, "Move method has not changed the default y-ordinate");

            //Test first draw
            testRender.DrawTo(200, 100);

            Assert.AreEqual(testRender.GetX_ord(), 200, "Draw method does not move to the correct x-ordinate");
            Assert.AreEqual(testRender.GetY_ord(), 100, "Draw method does not move to the correct y-ordinate");

            //Test second draw
            Assert.AreNotEqual(testRender.GetX_ord(), 0, "Draw method has not changed the default x-ordinate");
            Assert.AreNotEqual(testRender.GetY_ord(), 0, "Draw method has not changed the default y-ordinate");

            testRender.DrawTo(381, 245);

            Assert.AreEqual(testRender.GetX_ord(), 381, "Draw method does not move to the correct x-ordinate");
            Assert.AreEqual(testRender.GetY_ord(), 245, "Draw method does not move to the correct y-ordinate");
        }

        [TestMethod]
        public void Reset_RestoresDefualt()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Test current internal coordinates
            Render testRender = new Render(testGraphics);
            testRender.MoveTo(200, 100);

            Assert.AreNotEqual(testRender.GetX_ord(), 0, "Move method has not changed the default x-ordinate");
            Assert.AreNotEqual(testRender.GetY_ord(), 0, "Move method has not changed the default y-ordinate");

            //Test coordiantes returned to default
            testRender.Reset();

            Assert.AreEqual(testRender.GetX_ord(), 0, "Move method has not changed the default x-ordinate");
            Assert.AreEqual(testRender.GetY_ord(), 0, "Move method has not changed the default y-ordinate");
        }

        [TestMethod]
        public void SetColour_CorrectColour()
        {
            //Create arbetrary graphics context...
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();
            //... and testing tools
            KnownColor[] names = (KnownColor[])Enum.GetValues(typeof(KnownColor));
            Color testColour;

            //Test current internal pen
            Render testRender = new Render(testGraphics);
            Assert.AreEqual(testRender.GetPen().Color, Color.Black, "Pen not correctly initialised in output class");

            //Test changed internal pen
            testColour = Color.FromKnownColor( names[ rng.Next(28, 168) ] );
            testRender.SetColour(testColour);
            Assert.AreEqual(testRender.GetPen().Color, testColour, "Pen colour not correctly changed in output class");
        }

        [TestMethod]
        public void SetColour_MultipleChanges()
        {
            //Create arbetrary graphics context...
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();
            //... and testing tools
            KnownColor[] names = (KnownColor[])Enum.GetValues(typeof(KnownColor));
            Color testColour1, testColour2;

            //Test initial internal pen
            Render testRender = new Render(testGraphics);
            Assert.AreEqual(testRender.GetPen().Color, Color.Black, "Pen not correctly initialised in output class");

            //Test current internal pen
            testColour1 = Color.FromKnownColor(names[rng.Next(28, 168)]);
            testRender.SetColour(testColour1);
            Assert.AreEqual(testRender.GetPen().Color, testColour1, "Pen colour not correctly changed in output class");

            //Test changed internal pen
            testColour2 = Color.FromKnownColor(names[rng.Next(28, 168)]);
            testRender.SetColour(testColour2);
            Assert.AreEqual(testRender.GetPen().Color, testColour2, "Pen colour not correctly changed in output class");
        }

        [TestMethod]
        public void SetFill_On()
        {
            //Create arbetrary graphics context...
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Create graphics output object
            Render testRender = new Render(testGraphics);

            //Test turning fill on
            testRender.SetFill(true);
            Assert.IsTrue( testRender.GetFilling() );
        }

        [TestMethod]
        public void SetFill_Off()
        {
            //Create arbetrary graphics context...
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Create graphics output object
            Render testRender = new Render(testGraphics);

            //Test turning fill on
            testRender.SetFill(false);
            Assert.IsFalse(testRender.GetFilling());
        }
    }
}
