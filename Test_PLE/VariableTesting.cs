﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Programming_Learning_Environment;
using System.Windows.Forms;
using System.Drawing;

namespace Test_PLE
{
    [TestClass]
    public class VariableTesting
    {
        [TestMethod]
        public void Variable_Declare()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();
            Interpreter testInterpreter = new Interpreter(testGraphics);

            //Define 'code' line
            String testCode = "flibble = 10";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual( testInterpreter.InterpretLine(testCode), "ignore", "Variable has not been declared" );
        }

        [TestMethod]
        public void Variable_Update()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();
            Interpreter testInterpreter = new Interpreter(testGraphics);

            //Run 'code'
            testInterpreter.InterpretLine("flibble = 10");
            String testCode = "flibble = 5";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.InterpretLine(testCode), "ignore", "Variable has not been updated");
        }

        [TestMethod]
        public void Variable_Equation()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();
            Interpreter testInterpreter = new Interpreter(testGraphics);

            //Run 'code'
            String testDeclare = "flibble = 5+5";
            String testUpdate = "flibble = 10-5";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.InterpretLine(testDeclare), "ignore", "Variable has not been declared using an equation");
            Assert.AreEqual(testInterpreter.InterpretLine(testUpdate), "ignore", "Variable has not been updated using an equation");
        }

        public void Variable_Variables()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();
            Interpreter testInterpreter = new Interpreter(testGraphics);

            //Run 'code'
            testInterpreter.InterpretLine("a = 5");
            testInterpreter.InterpretLine("b = 10");

            String testDeclareC = "c = a";
            String testUpdateC = "c = b";

            String testDeclareD = "d = a + 5";
            String testUpdateD = "d = b- 5";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.InterpretLine(testDeclareC), "ignore", "Variable has not been declared using a variable");
            Assert.AreEqual(testInterpreter.InterpretLine(testUpdateC), "ignore", "Variable has not been updated using a variable");
            Assert.AreEqual(testInterpreter.InterpretLine(testDeclareD), "ignore", "Variable has not been declared using a variable in an expression");
            Assert.AreEqual(testInterpreter.InterpretLine(testUpdateD), "ignore", "Variable has not been updated using a variable in an expression");
        }

        [TestMethod]
        public void Variable_Method()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();
            Interpreter testInterpreter = new Interpreter(testGraphics);

            //Run 'code'
            testInterpreter.InterpretLine("flibble = 10");
            String testMethod = testInterpreter.ReplaceVariableInCommand( "rectangle flibble,20" );

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.InterpretCode(testMethod), "ignore", "Variable has not been updated using an equation");
        }
    }
}
