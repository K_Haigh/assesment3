﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Programming_Learning_Environment;
using System.Drawing;
using System.Windows.Forms;

namespace Test_PLE
{
    //Class testing 'Interpreter' class for all base/part 1 parsing functionality
    [TestClass]
    public class InterpreterTesting
    {
        //For robust testing of methods that should react to input form a given range.
        Random rng = new Random();

        [TestMethod]
        public void InterpretLine_moveto()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "moveto 200,100";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual( testInterpreter.InterpretLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious." );
        }

        [TestMethod]
        public void InterpretLine_drawto()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "drawto 200,100";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.InterpretLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void InterpretLine_setcolour()
        {
            //Create arbetrary graphics context...
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();
            //...and test resources
            KnownColor[] names = (KnownColor[])Enum.GetValues(typeof(KnownColor));

            //Define 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "setcolour " + names[ rng.Next(28, 168) ].ToString().ToLower();

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.InterpretLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void InterpretLine_fillon()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "fill on";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.InterpretLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void InterpretLine_filloff()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "fill off";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.InterpretLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void InterpretLine_circle()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "circle 74";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.InterpretLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void InterpretLine_triangleTwoSides()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "triangle 63,82";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.InterpretLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void InterpretLine_triangleThreeSides()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "triangle 63,82,43";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.InterpretLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void InterpretLine_rectangle()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "rectangle 63,82";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.InterpretLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void InterpretLine_polygon()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "polygon 6,100,100,150,100,150,150,150,200,200,200,300,300";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.InterpretLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void InterpretLine_clear()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "clear";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.InterpretLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void InterpretLine_reset()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "reset";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.InterpretLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }



        [TestMethod]
        public void InterpretCode_FromFile()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define 'code' source
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = System.IO.File.ReadAllText("Sample 'code'.txt");

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.InterpretCode(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }



        [TestMethod]
        public void CheckCommandLine_moveto()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "moveto 200,100";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.CheckCommandLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void CheckCommandLine_drawto()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "drawto 200,100";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.CheckCommandLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void CheckCommandLine_setcolour()
        {
            //Create arbetrary graphics context...
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();
            //...and test resources
            KnownColor[] names = (KnownColor[])Enum.GetValues(typeof(KnownColor));

            //Define 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "setcolour " + names[rng.Next(28,168)].ToString()/*.ToLower()*/;

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.CheckCommandLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void CheckCommandLine_fillon()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "fill on";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.CheckCommandLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void CheckCommandLine_filloff()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "fill off";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.CheckCommandLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void CheckCommandLine_circle()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "circle 74";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.CheckCommandLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void CheckCommandLine_triangleTwoSides()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "triangle 63,82";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.CheckCommandLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void CheckCommandLine_triangleThreeSides()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "triangle 63,82,43";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.CheckCommandLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void CheckCommandLine_rectangle()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "rectangle 63,82";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.CheckCommandLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void CheckCommandLine_polygon()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "polygon 6,100,100,150,100,150,150,150,200,200,200,300,300";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.CheckCommandLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void CheckCommandLine_clear()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "clear";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.CheckCommandLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void CheckCommandLine_reset()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Define 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "reset";

            //'Run' - correct 'execution' should return an "ignore" error code
            Assert.AreEqual(testInterpreter.CheckCommandLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }
    }
}
