﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Programming_Learning_Environment;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Test_PLE
{
    //Class testing for handling of expected end user vocabulary and syntax errors
    [TestClass]
    public class ErrorTesting
    {
        [TestMethod]
        public void Error_NoSpace()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Read 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "moveto200,100";
            string errorCode =  "Line: " + testCode + "\n" +
                                "This line does not follow the structure 'command' <space> 'data'\n\n" +
                                "Correct examples would be: 'moveto 100,100' OR 'setcolour red'\n" +
                                "OR 'clear', where no data is required";

            //'Run' - incorrect 'execution' should return a specific error code
            Assert.AreEqual(testInterpreter.CheckCommandLine(testCode), errorCode, "Entered command has not returned an error. Command should be invalid, handling code erronious.");
        }

        [TestMethod]
        public void Error_DoubleSpace()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Read 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "moveto  200,100";
            string errorCode =  "Line: " + testCode + "\n" +
                                "This line does not follow the structure 'command' <space> 'data'\n\n" +
                                "Correct examples would be: 'moveto 100,100' OR 'setcolour red'\n" +
                                "OR 'clear', where no data is required";

            //'Run' - incorrect 'execution' should return a specific error code
            Assert.AreEqual(testInterpreter.CheckCommandLine(testCode), errorCode, "Entered command has not returned an error. Command should be invalid, handling code erronious.");
        }

        [TestMethod]
        public void Error_SpaceNoData()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Read 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "moveto ";
            string errorCode =  "Data: " + testCode + "\n" +
                                "This is not valid data. It is not a recognised colour, or a whole number  written as an integer (1 or 2 or 10...).\n\n" +
                                "Valid colours can be found in help documentation, or online at:\n" +
                                "https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.colors";

            //'Run' - incorrect 'execution' should return a specific error code
            Assert.AreEqual(testInterpreter.CheckCommandLine(testCode), errorCode, "Entered command has not returned an error. Command should be invalid, handling code erronious.");
        }

        [TestMethod]
        public void Error_NoData()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Read 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "moveto ";
            string errorCode =  "Data: " + testCode + "\n" +
                                "This is not valid data. It is not a recognised colour, or a whole number  written as an integer (1 or 2 or 10...).\n\n" +
                                "Valid colours can be found in help documentation, or online at:\n" +
                                "https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.colors";

            //'Run' - incorrect 'execution' should return a specific error code
            Assert.AreEqual(testInterpreter.CheckCommandLine(testCode), errorCode, "Entered command has not returned an error. Command should be invalid, handling code erronious.");
        }

        [TestMethod]
        public void Error_InvalidCommand()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Read 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            string[] commands = new string[] { "moveto", "drawto", "setcolour", "fill", "circle", "triangle", "rectangle", "polygon", "clear", "reset" };
            String testCode = "flibble 100,100";
            string errorCode =  "Line: " + testCode + "\n" +
                                "This line does not contain a valid command\n\n" +
                                "Valid command words are:\n" + String.Join(", ", commands) + "\n\n" +
                                "If attempting to declare a variable, ensure that each term in the variable daclaration is seperated by a space.";

            //'Run' - incorrect 'execution' should return a specific error code
            Assert.AreEqual(testInterpreter.CheckCommandLine(testCode), errorCode, "Entered command has not returned an error. Command should be invalid, handling code erronious.");
        }

        [TestMethod]
        public void Error_IntNotString()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Read 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "setcolour 3";
            string errorCode =  "Line: " + testCode + "\n" +
                                "This command cannot take whole numbers as input. A worded colour must be given.\n\n" +
                                "Valid colours can be found in help documentation, or online at:\n" +
                                "https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.colors";

            //'Run' - incorrect 'execution' should return a specific error code
            Assert.AreEqual(testInterpreter.CheckCommandLine(testCode), errorCode, "Entered command has not returned an error. Command should be invalid, handling code erronious.");
        }

        [TestMethod]
        public void Error_StringNotInt()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Read 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "moveto onehundred,twohundred";
            string errorCode = "Data: " + testCode + "\n" +
                                "This is not valid data. It is not a recognised colour, or a whole number  written as an integer (1 or 2 or 10...).\n\n" +
                                "Valid colours can be found in help documentation, or online at:\n" +
                                "https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.colors";

            //'Run' - incorrect 'execution' should return a specific error code
            Assert.AreEqual(testInterpreter.CheckCommandLine(testCode), errorCode, "Entered command has not returned an error. Command should be invalid, handling code erronious.");
        }

        [TestMethod]
        public void Error_IntAndString()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Read 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "moveto 100,twohundred";
            string errorCode =  "Data: " + testCode + "\n\n" +
                                "The data entered does not entirely consist of whole numbers,  written as an integer (1 or 2 or 10...).\n" +
                                "Ensure every line only contains one type of data";

            //'Run' - incorrect 'execution' should return a specific error code
            Assert.AreEqual(testInterpreter.CheckCommandLine(testCode), errorCode, "Entered command has not returned an error. Command should be invalid, handling code erronious.");
        }

        [TestMethod]
        public void Error_StringAndInt()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Read 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "moveto onehundred,200";
            string errorCode =  "Data: " + testCode + "\n" +
                                "This is not valid data. It is not a recognised colour, or a whole number  written as an integer (1 or 2 or 10...).\n\n" +
                                "Valid colours can be found in help documentation, or online at:\n" +
                                "https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.colors";

            //'Run' - incorrect 'execution' should return a specific error code
            Assert.AreEqual(testInterpreter.CheckCommandLine(testCode), errorCode, "Entered command has not returned an error. Command should be invalid, handling code erronious.");
        }

        [TestMethod]
        public void Error_InvalidColour()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Read 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "setcolour deeppurple";
            string errorCode =  "Data: " + testCode + "\n" +
                                "This is not valid data. It is not a recognised colour, or a whole number  written as an integer (1 or 2 or 10...).\n\n" +
                                "Valid colours can be found in help documentation, or online at:\n" +
                                "https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.colors";

            //'Run' - incorrect 'execution' should return a specific error code
            Assert.AreEqual(testInterpreter.CheckCommandLine(testCode), errorCode, "Entered command has not returned an error. Command should be invalid, handling code erronious.");
        }

        [TestMethod]
        public void Error_WillNotFloat()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Read 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "moveto 200.1,100.9";
            string errorCode =  "Data: " + testCode + "\n" +
                                "This is not valid data. It is not a recognised colour, or a whole number  written as an integer (1 or 2 or 10...).\n\n" +
                                "Valid colours can be found in help documentation, or online at:\n" +
                                "https://docs.microsoft.com/en-us/dotnet/api/system.windows.media.colors";

            //'Run' - incorrect 'execution' should return a specific error code
            Assert.AreEqual(testInterpreter.CheckCommandLine(testCode), errorCode, "Entered command has not returned an error. Command should be invalid, handling code erronious.");
        }

        [TestMethod]
        public void Error_CircleNoRadius()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Read 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "circle";
            string errorCode =  "Line: " + testCode + "\n" +
                                "This line does not follow the structure 'command' <space> 'data'\n\n" +
                                "Correct examples would be: 'moveto 100,100' OR 'setcolour red'\n" +
                                "OR 'clear', where no data is required";

            //'Run' - incorrect 'execution' should return a specific error code
            Assert.AreEqual(testInterpreter.CheckCommandLine(testCode), errorCode, "Entered command has not returned an error. Command should be invalid, handling code erronious.");
        }

        [TestMethod]
        public void Error_ExtraData()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Read 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "moveto 100,100,100";

            //'Run' - additional integers are not used at all, therefor do not affect execution
            Assert.AreEqual(testInterpreter.InterpretLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void Error_PolygonHalfCoordinate()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Read 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "polygon 3,100,100,200,100,100,150,150";

            //'Run' - if an incomplete coordinate can be ignored (i.e. an extra number on the end by accident), it will be
            Assert.AreEqual(testInterpreter.InterpretLine(testCode), "ignore", "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void Error_NotAPolygon()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Read 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "polygon 1,100,100";
            string errorCode =  "Data: " + testCode + "\n" +
                                "This data is incomplete for the method 'polygon'.\n" +
                                "At least five whole numbers must be given - a number of vertices and two coordinates, each a pair of whole numbers.\n\n" +
                                "Every additional vertex (shape corner) must be given as an additional pair of whole numbers.";

            //'Run' - one point a line does not make
            Assert.AreEqual(testInterpreter.InterpretLine(testCode), errorCode, "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void Error_WonkyPolygonSmall()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Read 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "polygon 3,100,100,200,100";
            string errorCode =  "Data: " + testCode + "\n" +
                                "This data is incorrect for the described 'polygon'.\n\n" +
                                "The number of sides stated is " + "3" + "\n" +
                                "The number of coordinated given is " + "2";

            //'Run' - a three sided shape has more than two vertices
            Assert.AreEqual(testInterpreter.InterpretLine(testCode), errorCode, "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void Error_WonkyPolygonLarge()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Read 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "polygon 3,100,100,200,100,150,150,250,250";
            string errorCode =  "Data: " + testCode + "\n" +
                                "This data is incorrect for the described 'polygon'.\n\n" +
                                "The number of sides stated is " + "3" + "\n" +
                                "The number of coordinated given is " + "4";

            //'Run' - a three sided shape does not ahve four vertices
            Assert.AreEqual(testInterpreter.InterpretLine(testCode), errorCode, "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void Error_WonkyRectangle()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Read 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "rectangle 75";
            string errorCode =  "Data: " + testCode + "\n" +
                                "This data is incomplete for the method 'rectangle'. Two whole number lengths must be specified.";

            //'Run' - one length a rectangle does not make
            Assert.AreEqual(testInterpreter.InterpretLine(testCode), errorCode, "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void Error_WonkyTriangle()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Read 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "triangle 75";
            string errorCode =  "Data: " + testCode + "\n" +
                                "This data is incomplete for the method 'triangle'. At least two whole number lengths must be specified.\n" +
                                "Alternatively, two whole number lengths and a whole number angle, measured in degrees.";

            //'Run' - one length a line traingle not make
            Assert.AreEqual(testInterpreter.InterpretLine(testCode), errorCode, "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void Error_WonkyMove()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Read 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "moveto 75";
            string errorCode =  "Data: " + testCode + "\n" +
                                "This data is incomplete for the method 'moveto'. A coordinate must be specified consisting of two whole numbers.";

            //'Run' - one ordinate a coordinate does not make
            Assert.AreEqual(testInterpreter.InterpretLine(testCode), errorCode, "Entered command has returned an error. Command should be valid, handling code erronious.");
        }

        [TestMethod]
        public void Error_WonkyDraw()
        {
            //Create arbetrary graphics context
            PictureBox testControl = new System.Windows.Forms.PictureBox();
            Graphics testGraphics = testControl.CreateGraphics();

            //Read 'code' line
            Interpreter testInterpreter = new Interpreter(testGraphics);
            String testCode = "drawto 75";
            string errorCode =  "Data: " + testCode + "\n" +
                                "This data is incomplete for the method 'drawto'. A coordinate must be specified consisting of two whole numbers.";

            //'Run' - one ordinate a coordinate does not make
            Assert.AreEqual(testInterpreter.InterpretLine(testCode), errorCode, "Entered command has returned an error. Command should be valid, handling code erronious.");
        }
    }
}
