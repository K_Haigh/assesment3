﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test_PLE
{
    class IfTesting
    {
        /*
         * Interpretation of user code including a 'if' requires use of Programming_Window class, RunCommand method.
         * This is the method that handles complex use of variables (more than assigning values)
         * As a Singleton (thus a static class), cannot be ascessed from a Test class
        */

        [TestMethod]
        public void If_Method()
        {
            //" name 'Programming_Window' does not exist in the current context"
            //Programming_Window.RunCommand()
        }
    }
}
