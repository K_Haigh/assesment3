﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Programming_Learning_Environment;
using System;

namespace Test_PLE
{
    [TestClass]
    public class Programming_WindowTesting
    {
        [TestMethod]
        public void ProgrammingWindow_Variables()
        {
            //Create instance of program (GUI & handling code)
            Programming_Window testWindow = Programming_Window.GetInstancePW();
            String testCode = System.IO.File.ReadAllText("Variables demo.txt");

            //Define user entered code
            testWindow.SetCode(testCode);
            string[] linesIn = Programming_Window.GetCode();

            //'Run' user entered code
            int i = 0;
            Boolean canRun;
            do
            {
                if (i < linesIn.Length)
                {
                    canRun = testWindow.RunCommand(linesIn[i]);

                    Assert.AreEqual(canRun, true, "This line could not be run: " + linesIn[i]);

                    i++;
                }
                else
                {
                    canRun = false;
                }
            }
            while (canRun);
        }
        [TestMethod]
        public void ProgrammingWindow_While()
        {
            //Create instance of program (GUI & handling code)
            Programming_Window testWindow = Programming_Window.GetInstancePW();
            String testCode = System.IO.File.ReadAllText("While demo.txt");

            //Define user entered code
            testWindow.SetCode(testCode);
            string[] linesIn = Programming_Window.GetCode();

            //'Run' user entered code
            int i = 0;
            Boolean canRun;
            do
            {
                if (i < linesIn.Length)
                {
                    canRun = testWindow.RunCommand(linesIn[i]);

                    Assert.AreEqual(canRun, true, "This line could not be run: " + linesIn[i]);

                    i++;
                }
                else
                {
                    canRun = false;
                }
            }
            while (canRun);
        }

        [TestMethod]
        public void ProgrammingWindow_If()
        {
            //Create instance of program (GUI & handling code)
            Programming_Window testWindow = Programming_Window.GetInstancePW();
            String testCode = System.IO.File.ReadAllText("If demo.txt");

            //Define user entered code
            testWindow.SetCode(testCode);
            string[] linesIn = Programming_Window.GetCode();

            //'Run' user entered code
            int i = 0;
            Boolean canRun;
            do
            {
                if (i < linesIn.Length)
                {
                    canRun = testWindow.RunCommand(linesIn[i]);

                    Assert.AreEqual(canRun, true, "This line could not be run: " + linesIn[i]);

                    i++;
                }
                else
                {
                    canRun = false;
                }
            }
            while (canRun);
        }

        [TestMethod]
        public void ProgrammingWindow_Method()
        {
            //Create instance of program (GUI & handling code)
            Programming_Window testWindow = Programming_Window.GetInstancePW();
            String testCode = System.IO.File.ReadAllText("Method demo.txt");

            //Define user entered code
            testWindow.SetCode(testCode);
            string[] linesIn = Programming_Window.GetCode();

            //'Run' user entered code
            int i = 0;
            Boolean canRun;
            do
            {
                if (i < linesIn.Length)
                {
                    canRun = testWindow.RunCommand(linesIn[i]);

                    Assert.AreEqual(canRun, true, "This line could not be run: " + linesIn[i]);

                    i++;
                }
                else
                {
                    canRun = false;
                }
            }
            while (canRun);
        }

        [TestMethod]
        public void ProgrammingWindow_AllTogetherNow()
        {
            //Create instance of program (GUI & handling code)
            Programming_Window testWindow = Programming_Window.GetInstancePW();

            //Define user entered code
            String testMethod = System.IO.File.ReadAllText("Method demo.txt");
            String testLoop = System.IO.File.ReadAllText("Loopy method.txt");
            string[] linesIn;

            //'Run'
            testWindow.SetCode(testMethod);
            linesIn = Programming_Window.GetCode();
            Run(linesIn, testWindow);

            testWindow.SetCode(testLoop);
            linesIn = Programming_Window.GetCode();
            Run(linesIn, testWindow);
        }
 


        //Keep test cases more consise
        private void Run(string[] linesIn, Programming_Window testWindow)
        {
            int i = 0;
            Boolean canRun;
            do
            {
                if (i < linesIn.Length)
                {
                    canRun = testWindow.RunCommand(linesIn[i]);

                    Assert.AreEqual(canRun, true, "This line could not be run: " + linesIn[i]);

                    i++;
                }
                else
                {
                    canRun = false;
                }
            }
            while (canRun);
        }
    }
}
